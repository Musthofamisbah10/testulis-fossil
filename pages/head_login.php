
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
    <!-- Bootstrap Core CSS -->
   <link rel="stylesheet" href="css/login.css">

	<?php 
	if(isset($_GET['p'])) {
		$page = $_GET['p'];
		if($page=='home') {
			$title= 'Login Member - TesTulis FOSSIL';
		} else if($page=='admin_login'){
			$title= 'Login Admin - TesTulis FOSSIL';
		} else if($page=='pengurus_login'){
			$title= 'Login Pengurus - TesTulis FOSSIL';
		} else {
			$title= 'Halaman Tidak Ditemukan - TesTulis FOSSIL';
		}
		
	}else {
			$title= 'Login Member - TesTulis FOSSIL';
		}
	?>

    <title><?php echo $title?></title>