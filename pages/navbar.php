
<header class="topbar">
	<nav class="navbar top-navbar navbar-toggleable-sm navbar-light">
        <div class="navbar-header">
            <a class="navbar-brand" href="home">
            	<span>
                	<img src="assets/images/TT.png" width="85%" alt="homepage" class="dark-logo" />
            	</span>
            </a>
        </div>
        <div class="navbar-collapse">
            <ul class="navbar-nav mr-auto mt-md-0 "></ul>
                <?php 
				   if($pages_dir == "member") {
						$foto = get_member_all($_SESSION['idne'])[3];
					} else if($pages_dir == "pengurus") {
						$foto =  get_pengurus_alldata_from_id($_SESSION['idne'])[4];
					} else if($pages_dir == "admin") {
						$foto =  get_admin_alldata_from_id($_SESSION['idne'])[3];
					}  
				   
				?>
            <ul class="navbar-nav my-lg-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class='user-im' src="assets/images/users/<?php echo $foto ?>" alt="user" class="profile-pic m-r-5" />
					<?php
						if($pages_dir == "member") {
							echo get_nama_member($uname);
						} else if($pages_dir == "pengurus") {
							echo get_nama_pengurus($uname);
						} else if($pages_dir == "admin") {
							echo get_nama_admin($uname);
						}  		
					?> 
							
					<i class="fa fa-angle-down" aria-hidden="true"></i></a> 
						<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
							<a class="dropdown-item" href="edit_profil">Edit Profil</a>
							<a class="dropdown-item" href="signout">Logout</a>
						</div>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
      