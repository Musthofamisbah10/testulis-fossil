<?php

//fungsi mengambil nama member
function get_nama_member($uname) {
$q = "SELECT nama_member FROM member where uname_member='$uname'";
$rs = mysql_query($q);
$dt = mysql_fetch_array($rs);
return $dt['nama_member'];
}

//fungsi memanggil nama pengurus
function get_nama_pengurus($uname) {
$q = "SELECT nama_pengurus FROM pengurus where uname_pengurus='$uname'";
$rs = mysql_query($q);
$dt = mysql_fetch_array($rs);
return $dt['nama_pengurus'];
}

//fungsi memanggil nama admin
function get_nama_admin($uname) {
$q = "SELECT nama_admin FROM admin where uname_admin='$uname'";
$rs = mysql_query($q);
$dt = mysql_fetch_array($rs);
return $dt['nama_admin'];
}

//get ujian Tulis
function get_ujian_tulis() {
	$q = "SELECT nama_ujian, id_pengurus, id_ujian, id_materi  FROM ujian where aktif=1";
	$rs = mysql_query($q);
	while($row = mysql_fetch_array($rs)) {
		$dt[] = $row['nama_ujian'];
		$dt1[] = $row['id_pengurus'];
		$dt2[] = $row['id_ujian'];
		$dt3[] = $row['id_materi'];
		
	}
	return array($dt,$dt1,$dt2,$dt3);
}

//get_pengurus from id
function get_pengurus_from_id($id) {
	$q = "SELECT nama_pengurus FROM pengurus where id_pengurus='$id'";
	$rs = mysql_query($q);
	$dt = mysql_fetch_array($rs);
	return $dt['nama_pengurus'];
}

//get nama ujian berdasar id
function get_ujian_from_id($id) {
	$q = "SELECT * FROM ujian where id_ujian='$id'";
	$rs = mysql_query($q);
	$dt = mysql_fetch_array($rs);
	return $dt;
}

//fungsi get materi
function get_materi_from_id($id) {
	$q = "SELECT * FROM materi where id_materi='$id'";
	$rs = mysql_query($q);
	$dt = mysql_fetch_array($rs);
	return $dt;
}

//get semua materi
function get_materi_all() {
	$q = "SELECT id_materi, nama_materi FROM materi";
	$rs = mysql_query($q);
	while($row = mysql_fetch_array($rs)) {
		$dt[] = $row['id_materi'];
		$dt1[] = $row['nama_materi'];
	}
return array($dt,$dt1);
}

//ambil data semua pengurus
function get_pengurus_all() {
	$q = "SELECT nama_pengurus, uname_pengurus, id_materi, id_pengurus FROM pengurus";
	$rs = mysql_query($q);
	while($row = mysql_fetch_array($rs)) {
		$dt[] = $row['nama_pengurus'];
		$dt1[] = $row['uname_pengurus'];
		$dt2[] =  $row['id_materi'];
		$dt3[] = $row['id_pengurus'];
	}
return array($dt,$dt1,$dt2,$dt3);
}

//ambil semua data pengurus berdasarkan id
function get_pengurus_alldata_from_id($id) {
	$q = "SELECT nama_pengurus, uname_pengurus, id_materi, id_pengurus, foto FROM pengurus where id_pengurus='$id'";
	$rs = mysql_query($q);
	$row = mysql_fetch_array($rs);
		$dt = $row['nama_pengurus'];
		$dt1 = $row['uname_pengurus'];
		$dt2 =  $row['id_materi'];
		$dt3 = $row['id_pengurus'];
		$dt4 = $row['foto'];
return array($dt,$dt1,$dt2,$dt3,$dt4);
}

//get semua member
function get_member_all($id) {
	if($id!=0) {
		$q = "SELECT nama_member, uname_member, kelas, foto FROM member where id_member='$id'";
	$rs = mysql_query($q);
	$row = mysql_fetch_array($rs);
		$dt = $row['nama_member'];
		$dt1 = $row['uname_member'];
		$dt2 =  $row['kelas'];
		$dt3 = $row['foto'];
		return array($dt,$dt1,$dt2,$dt3);

	} else if($id==0){
	$q = "SELECT nama_member, uname_member, kelas, id_member  FROM member";
	$rs = mysql_query($q);
	while($row = mysql_fetch_array($rs)) {
		$dt[] = $row['nama_member'];
		$dt1[] = $row['uname_member'];
		$dt2[] =  $row['kelas'];
		$dt3[] = $row['id_member'];
	}
	return array($dt,$dt1,$dt2,$dt3);
	}
}

//get nama kelas berdasarkan id
function get_kelas_from_id($id) {
	$q = "SELECT nama_kelas FROM kelas where id_kelas='$id'";
	$rs = mysql_query($q);
	$dt = mysql_fetch_array($rs);
	return $dt['nama_kelas'];
}

//get semua kelas
function get_kelas_all() {
	$q = "SELECT nama_kelas, id_kelas FROM kelas";
	$rs = mysql_query($q);
	while($row = mysql_fetch_array($rs)) {
		$dt[] = $row['nama_kelas'];
		$dt1[] = $row['id_kelas'];
	}
		
return array($dt,$dt1);
}

// get semua kamus kata dasar
function get_kamus_all() {
	$q = "SELECT dasar, id_dasar FROM kamus";
	$rs = mysql_query($q);
	while($row = mysql_fetch_array($rs)) {
		$dt[] = $row['dasar'];
		$dt1[] = $row['id_dasar'];
	}
		
return array($dt,$dt1);
}

// get semua kamus kata hubung
function get_kata_hubung_all() {
	$q = "SELECT kata, id_kata FROM katahubung";
	$rs = mysql_query($q);
	while($row = mysql_fetch_array($rs)) {
		$dt[] = $row['kata'];
		$dt1[] = $row['id_kata'];
	}
		
return array($dt,$dt1);
}

//get materi berdasarkan id
function get_study_from_id($id) {
	$q = "SELECT nama_materi FROM materi where id_materi='$id'";
	$rs = mysql_query($q);
	$dt = mysql_fetch_array($rs);
	return $dt['nama_materi'];
}

// get semua materi
function get_study_all() {
	$q = "SELECT nama_materi, id_materi FROM materi";
	$rs = mysql_query($q);
	while($row = mysql_fetch_array($rs)) {
		$dt[] = $row['nama_materi'];
		$dt1[] = $row['id_materi'];
	}
	return array($dt,$dt1);
}

//get semua ujian
function get_ujian_all($id) {
	if($id!=0) {
		$q = "SELECT * FROM ujian where id_ujian='$id'";
	$rs = mysql_query($q);
	$row = mysql_fetch_array($rs);
		$dt = $row['nama_ujian'];
		$dt1 = $row['id_materi'];
		$dt2 = $row['id_pengurus'];
		$dt3 = $row['token_ujian'];
		$dt4 = $row['aktif'];
		$dt5 = $row['id_ujian'];

		return array($dt,$dt1,$dt2,$dt3,$dt4,$dt5);
	} else if($id==0){
	$q = "SELECT * FROM ujian";
	$rs = mysql_query($q);
	while($row = mysql_fetch_array($rs)) {
		$dt[] = $row['nama_ujian'];
		$dt1[] = $row['id_materi'];
		$dt2[] = $row['id_pengurus'];
		$dt3[] = $row['token_ujian'];
		$dt4[] = $row['aktif'];
		$dt5[] = $row['id_ujian'];
	}
	return array($dt,$dt1,$dt2,$dt3,$dt4,$dt5);
	}
}

//get ujian dari pegurus
function get_ujian_from_pengurus($id) {
	
	$q = "SELECT * FROM ujian where id_pengurus='$id'";
	$rs = mysql_query($q);
	while($row = mysql_fetch_array($rs)) {
		$dt[] = $row['nama_ujian'];
		$dt1[] = $row['id_materi'];
		$dt2[] = $row['id_pengurus'];
		$dt3[] = $row['token_ujian'];
		$dt4[] = $row['aktif'];
		$dt5[] = $row['id_ujian'];
		
	}
	if(isset($dt)!=null) {
	return array($dt,$dt1,$dt2,$dt3,$dt4,$dt5);
	} else 
	
	{return null;}
	
	
}


//fugnsi get soal dari ujian 
function get_soal_from_ujian($id) {
	$q = "SELECT soal, kunci_soal, id_soal FROM soal where id_ujian='$id'";
	$rs = mysql_query($q);
	while($row = mysql_fetch_array($rs)) {
		$dt[] = $row['soal'];
		$dt1[] = $row['kunci_soal'];
		$dt2[] =  $row['id_soal'];
		
	}
	if(isset($dt)!=null) {
	return array($dt,$dt1,$dt2);
	} else 
	
	{return null;}
}

//get soal berdasarkan id
function get_soal_from_id($id) {
	$q = "SELECT soal,kunci_soal, id_ujian FROM soal where id_soal='$id'";
	$rs = mysql_query($q);
	$row = mysql_fetch_array($rs);
		$dt = $row['soal'];
		$dt1 = $row['kunci_soal'];
		$dt2 =  $row['id_ujian'];
		
return array($dt,$dt1,$dt2);
}

// get semua admin
function get_admin_all() {
	$q = "SELECT nama_admin, uname_admin, id_admin FROM admin";
	$rs = mysql_query($q);
	while($row = mysql_fetch_array($rs)) {
		$dt[] = $row['nama_admin'];
		$dt1[] = $row['uname_admin'];
		$dt2[] = $row['id_admin'];
	}
return array($dt,$dt1,$dt2);
}

// get semua data admin
function get_admin_alldata_from_id($id) {
	$q = "SELECT * FROM admin where id_admin='$id'";
	$rs = mysql_query($q);
	$row = mysql_fetch_array($rs);
		$dt = $row['nama_admin'];
		$dt1 = $row['uname_admin'];
		$dt2 = $row['id_admin'];
		$dt3 = $row['foto'];
return array($dt,$dt1,$dt2,$dt3);
}

//get hasil ujian di pengurus
function get_hasil_ujian($id) {
	$q = "SELECT id_ujian, id_member, nilai_ujian  FROM hasil_ujian where id_ujian in (SELECT id_ujian FROM ujian WHERE id_pengurus=$id)";
	$rs = mysql_query($q);
	while($row = mysql_fetch_array($rs)) {
		$dt[] = $row['id_ujian'];
		$dt1[] = $row['id_member'];
		$dt2[] = $row['nilai_ujian'];
	}
	if(isset($dt)) {
return array($dt,$dt1,$dt2);
	}
	return 0;

}

//get hasil ujian berdasarkan id member
function get_hasil_ujian_member($id) {
	$q = "SELECT id_ujian, nilai_ujian FROM hasil_ujian where id_member = '$id'";
	$rs = mysql_query($q);
	while($row = mysql_fetch_array($rs)) {
		$dt[] = $row['id_ujian'];
		$dt1[] = $row['nilai_ujian'];
	}
	if(isset($dt)) {
return array($dt,$dt1);
	}
	return 0;
}

//cek status ujian member
function cek_status_ujian_member($id_member, $id_ujian) {
	$q = "SELECT id_member, id_ujian FROM hasil_ujian where id_member = '$id_member' AND id_ujian='$id_ujian'" ;
	$rs = mysql_query($q);
	if(mysql_num_rows($rs)>0) {
		return 0;
	} else {
		return 1;
	}
}

//status ujian
function status_ujian($no) {
	if($no==1) {
		$status = 'Aktif';
	} else {
		$status = 'Tidak Aktif';
	}
	return $status;
}

//random token ujian
function token($panjang){
   $pengacak = 'ABCDEFGHIJKLMNOPQRSTU1234567890';
   $string = '';
   for($i = 0; $i < $panjang; $i++) {
   $pos = rand(0, strlen($karakter)-1);
   $string .= $karakter{$pos};
   }
    return $string;
}

//pecah waktu
function pecah_time($time) {
	$data = explode(":",$time);
	$jam = ltrim($data[0], '0');
	$menit = ltrim($data[1], '0');
	if($jam>0&&$menit!=null) {
		echo $jam." jam ".$menit." menit";
	} else if($menit<=0||$menit==null) {
		echo $jam." jam ";
	} else {
		echo $menit." menit";
	}
}

/////////////////////////////////////
//FUNGSI PENCOCOKAN + ALG RABINKARP//
///////////////////////////////////////


//membersihkan simbol dan angka (Case Folding)
function hapus_simbol($string) {
   $string = strtolower($string);
   $string2 = preg_replace('/[^A-Za-z0-9\-]/', ' ', $string);
   return preg_replace('/\s+/', ' ',$string2);
}

//memecah per kata (Tonizing)
function pecah_kata($kalimat) {
	$data = explode(" ",$kalimat);
	return $data;
}

// fungsi random karakter
function random($length) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}


////////////////////////////////////////
/// STEMMING  fungsi START  //////////
////////////////////////////////

// fungsi cek kamus dasar
function cekKamus($kata){ 
	  $sql = mysql_query("SELECT dasar from kamus where dasar ='$kata' LIMIT 1");
	  $result = mysql_num_rows($sql);
	
	 if($result==1){
		return true; // True jika ada
	 }else{
		 return false; // jika tidak ada FALSE
	 }
}

//fungsi untuk menghapus suffix seperti -ku, -mu, -kah, dsb
function Del_Inflection_Suffixes($kata){ 
	$kataAsal = $kata;
	
	if(preg_match('/([km]u|nya|[kl]ah|pun)\z/i',$kata)){ // Cek Inflection Suffixes
		$__kata = preg_replace('/([km]u|nya|[kl]ah|pun)\z/i','',$kata);

		return $__kata;
	}
	return $kataAsal;
}

// Hapus Derivation Suffixes ("-i", "-an" atau "-kan")
function Del_Derivation_Suffixes($kata){
	$kataAsal = $kata;
	if(preg_match('/(i|an)\z/i',$kata)){ // Cek Suffixes
		$__kata = preg_replace('/(i|an)\z/i','',$kata);
		if(cekKamus($__kata)){ // Cek Kamus
			return $__kata;
		}else if(preg_match('/(kan)\z/i',$kata)){
			$__kata = preg_replace('/(kan)\z/i','',$kata);
			if(cekKamus($__kata)){
				return $__kata;
			}
		}

		/*– Jika Tidak ditemukan di kamus –*/
	}
	return $kataAsal;
}

// Hapus Derivation Prefix ("di-", "ke-", "se-", "te-", "be-", "me-", atau "pe-")
function Del_Derivation_Prefix($kata){
	$kataAsal = $kata;

	/* —— Tentukan Tipe Awalan ————*/
	if(preg_match('/^(di|[ks]e)/',$kata)){ // Jika di-,ke-,se-
		$__kata = preg_replace('/^(di|[ks]e)/','',$kata);
		
		if(cekKamus($__kata)){
			return $__kata;
		}
		
		$__kata__ = Del_Derivation_Suffixes($__kata);
			
		if(cekKamus($__kata__)){
			return $__kata__;
		}
		
		if(preg_match('/^(diper)/',$kata)){ //diper-
			$__kata = preg_replace('/^(diper)/','',$kata);
			$__kata__ = Del_Derivation_Suffixes($__kata);
		
			if(cekKamus($__kata__)){
				return $__kata__;
			}
			
		}
		
		if(preg_match('/^(ke[bt]er)/',$kata)){  //keber- dan keter-
			$__kata = preg_replace('/^(ke[bt]er)/','',$kata);
			$__kata__ = Del_Derivation_Suffixes($__kata);
		
			if(cekKamus($__kata__)){
				return $__kata__;
			}
		}
			
	}
	
	if(preg_match('/^([bt]e)/',$kata)){ //Jika awalannya adalah "te-","ter-", "be-","ber-"
		
		$__kata = preg_replace('/^([bt]e)/','',$kata);
		if(cekKamus($__kata)){
			return $__kata; // Jika ada balik
		}
		
		$__kata = preg_replace('/^([bt]e[lr])/','',$kata);	
		if(cekKamus($__kata)){
			return $__kata; // Jika ada balik
		}	
		
		$__kata__ = Del_Derivation_Suffixes($__kata);
		if(cekKamus($__kata__)){
			return $__kata__;
		}
	}
	
	if(preg_match('/^([mp]e)/',$kata)){
		$__kata = preg_replace('/^([mp]e)/','',$kata);
		if(cekKamus($__kata)){
			return $__kata; // Jika ada balik
		}
		$__kata__ = Del_Derivation_Suffixes($__kata);
		if(cekKamus($__kata__)){
			return $__kata__;
		}
		
		if(preg_match('/^(memper)/',$kata)){
			$__kata = preg_replace('/^(memper)/','',$kata);
			if(cekKamus($kata)){
				return $__kata;
			}
			$__kata__ = Del_Derivation_Suffixes($__kata);
			if(cekKamus($__kata__)){
				return $__kata__;
			}
		}
		
		if(preg_match('/^([mp]eng)/',$kata)){
			$__kata = preg_replace('/^([mp]eng)/','',$kata);
			if(cekKamus($__kata)){
				return $__kata; // Jika ada balik
			}
			$__kata__ = Del_Derivation_Suffixes($__kata);
			if(cekKamus($__kata__)){
				return $__kata__;
			}
			
			$__kata = preg_replace('/^([mp]eng)/','k',$kata);
			if(cekKamus($__kata)){
				return $__kata; // Jika ada balik
			}
			$__kata__ = Del_Derivation_Suffixes($__kata);
			if(cekKamus($__kata__)){
				return $__kata__;
			}
		}
		
		if(preg_match('/^([mp]eny)/',$kata)){
			$__kata = preg_replace('/^([mp]eny)/','s',$kata);
			if(cekKamus($__kata)){
				return $__kata; // Jika ada balik
			}
			$__kata__ = Del_Derivation_Suffixes($__kata);
			if(cekKamus($__kata__)){
				return $__kata__;
			}
		}
		
		if(preg_match('/^([mp]e[lr])/',$kata)){
			$__kata = preg_replace('/^([mp]e[lr])/','',$kata);
			if(cekKamus($__kata)){
				return $__kata; // Jika ada balik
			}
			$__kata__ = Del_Derivation_Suffixes($__kata);
			if(cekKamus($__kata__)){
				return $__kata__;
			}
		}
		
		if(preg_match('/^([mp]en)/',$kata)){
			$__kata = preg_replace('/^([mp]en)/','t',$kata);
			if(cekKamus($__kata)){
				return $__kata; // Jika ada balik
			}
			$__kata__ = Del_Derivation_Suffixes($__kata);
			if(cekKamus($__kata__)){
				return $__kata__;
			}
			
			$__kata = preg_replace('/^([mp]en)/','',$kata);
			if(cekKamus($__kata)){
				return $__kata; // Jika ada balik
			}
			$__kata__ = Del_Derivation_Suffixes($__kata);
			if(cekKamus($__kata__)){
				return $__kata__;
			}
		}
			
		if(preg_match('/^([mp]em)/',$kata)){
			$__kata = preg_replace('/^([mp]em)/','',$kata);
			if(cekKamus($__kata)){
				return $__kata; // Jika ada balik
			}
			$__kata__ = Del_Derivation_Suffixes($__kata);
			if(cekKamus($__kata__)){
				return $__kata__;
			}
			
			$__kata = preg_replace('/^([mp]em)/','p',$kata);
			if(cekKamus($__kata)){
				return $__kata; // Jika ada balik
			}
			
			$__kata__ = Del_Derivation_Suffixes($__kata);
			if(cekKamus($__kata__)){
				return $__kata__;
			}
		}	
	}
	// jika tidak ada kembali ke kata asal
	return $kataAsal;
}


//////////////////////////
/// STEMING FUNGSI END
///////////////////////////////////


// fungsi cari kata dasar
function cari_kata_dasar($kata){ 

	$kataAsal = $kata;

	$cekKata = cekKamus($kata);
	if($cekKata == true){ // Cek Kamus
		return $kata; // Jika Ada maka kata tersebut adalah kata dasar
	}else{ //jika tidak ada dalam kamus maka dilakukan stemming
		$kata = Del_Inflection_Suffixes($kata);
		if(cekKamus($kata)){
			return $kata;
		}
		
		$kata = Del_Derivation_Suffixes($kata);
		if(cekKamus($kata)){
			return $kata;
		}
		
		$kata = Del_Derivation_Prefix($kata);
		if(cekKamus($kata)){
			return $kata;
		}
	}
}

//hapus spasi
function hapus_spasi($stringg) {
 
$stringg = str_replace(' ', '', $stringg);
  return $stringg;
}

//mecah data menjadi berdasarkan $n 
function pecah_data($dasar, $n) {
        $ngrams = array();
        $len = strlen($dasar);
        for($i = 0; $i < $len; $i++) {
                if($i > ($n - 2)) {
                        $ng = '';
                        for($j = $n-1; $j >= 0; $j--) {
                                $ng .= $dasar[$i-$j]; 
                        }
                        $ngrams[] = konversi_hash($ng);
                }
        }
        return $ngrams;
}

//fungsi mengubah ke nilai hash
function konversi_hash($key)
    {
       $charArray = str_split($key);
        $hash = 0;
      $sum = 0;
       $result = array();
        $jml = count($charArray);
        foreach($charArray as $char)
        {
                        $jml--;
            
             $hash = ord($char);
            
            $z= ($hash * pow(10,$jml));
			//echo $z."+".$jml."  ";
            //$result[] = "( ".$hash ." * 10^".$jml." )";
           
            $sum = $sum+$z;;
           
        }
		//echo $sum;
      //$x =$sum % 101;
       // return $x;
		return $sum;
    }
	
	
//fungsi algo rabinkarp
function rabin_karp($input, $kgrams) {
	
	$data_pecah = pecah_kata(hapus_simbol($input));
	for($i=0;$i<count($data_pecah);$i++){
     
         $result=mysql_query("SELECT kata FROM katahubung WHERE kata = '$data_pecah[$i]'");
		 $jml=mysql_num_rows($result); 
		
			if($jml>0){
				$cek_filtering[$i] = '';
			}else{
				$cek_filtering[$i] = $data_pecah[$i];
			};
			
	}           
	$hasil_filtering = strtolower(implode(" ",$cek_filtering));
	$array_data_filtering = explode(" ",$hasil_filtering);
	$data_steam = array();
	
    foreach($array_data_filtering as $katasteaming) {	
			$data_steam[] = cari_kata_dasar($katasteaming);
			$i++;
	}
	
	$x1= implode(' ',$data_steam);
	
	$data = hapus_spasi($x1);
	$pecahan_data =pecah_data("$data","$kgrams");
	
	return $pecahan_data;
}

//mencari hasil stem untuk pengurus
function get_stem($input) {
	$kgrams = 3;
	$data_pecah = pecah_kata(hapus_simbol($input));
	for($i=0;$i<count($data_pecah);$i++){
     
         $result=mysql_query("SELECT kata FROM katahubung WHERE kata = '$data_pecah[$i]'");
		 $jml=mysql_num_rows($result); 
		
			if($jml>0){
				$cek_filtering[$i] = '';
			}else{
				$cek_filtering[$i] = $data_pecah[$i];
			};
			
	}           
	$hasil_filtering = strtolower(implode(" ",$cek_filtering));
	$array_data_filtering = explode(" ",$hasil_filtering);
	$data_steam = array();
	
    foreach($array_data_filtering as $katasteaming) {	
			$data_steam[] = cari_kata_dasar($katasteaming);
			$i++;
	}
	
	$x1= implode(' ',$data_steam);
	$data = hapus_spasi($x1);
	return $data;
}

//haspus spasi & make array
function buat_array($x1) {
	$kgrams = 3;
	$data = hapus_spasi($x1);
	$pecahan_data =pecah_data("$data","$kgrams");
	
	return $pecahan_data;
}

//2 digit dibelakang koma
function dua_digit($number) {
return number_format((float)$number, 2, '.', '');
}