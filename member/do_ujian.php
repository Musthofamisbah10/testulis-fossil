	<title>Mengerjakan Ujian - TesTulis FOSSIL</title>

	<!-- get data ujian -->

	<?php

	$idujian = $_GET['iduji'];
	$ujian = get_ujian_from_id($_GET['iduji']);

	?>

	<!-- link JS -->
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/action/ujian.js"></script>

	<div id="main-wrapper">
	
	<!-- bar -->
	<?php require_once "pages/navbar.php";?>
    <?php require_once "pages/sidebar.php";?>

		<div class ="page-wrapper">
			<div class ="container-fluid">
				<div class ="row page-titles">
					<div class ="col-md-6 col-8 align-self-center">
						<h3 class ="text-themecolor m-bm0 m-t-0"><?php echo $ujian['nama_ujian'];?></h3>
						<ol class ="breadcrumb">
							<li class ="breadcrumb-item"><a href="home">Home</a></li>
							<li class ="breadcrumb-item active">Ujian</li>
							<li class ="breadcrumb-item active"><?php echo $ujian['nama_ujian'];?></li>
						</ol>
					</div>
				</div>
				<div class ="row">
					<div class ="col-sm-12">
						<div class ="card">
							<div class ="card-block">
								<div id ="intro_ujian">
									<h4 class ="card-title">Mengerjakan Ujian</h4>
									<span class ="text-muted">Detail Ujian:</span>
									<div class ="table-responsive">
										<table class ="table" border='0px'>
											<tr>
												<td><b>Nama Ujian</b></td>
												<td>:</td>
												<td><?php echo $ujian['nama_ujian'];?></td>
											</tr>
											<tr>
												<td><b>Materi</b></td>
												<td>:</td>
												<td><?php echo get_materi_from_id($ujian['id_materi'])['nama_materi'];?></td>
											</tr>
											<tr>
												<td><b>Pengurus</b></td>
												<td>:</td>
												<td><?php echo get_pengurus_from_id($ujian['id_pengurus']);?></td>
											</tr>
											<tr>
												<td><b>Token Ujian</b></td>
												<td>:</td>
												<td>
													<div class="col-sm-3" style='margin:0px;padding:0px;'>
														<input class ="form-control form-control-line" type="text" name ="tokenujian" placeholder ="Masukan Token">
														<input class ="form-control form-control-line" type="text" name ="idujian" value ="<?php echo $idujian?>" style="display:none;">
													</div>
												</td>
											</tr>
										</table>
									</div>
									<div class ="form-group">
										<div class ="col-sm-12">
											<?php
												if(cek_status_ujian_member($_SESSION['idne'], $idujian)){
													echo "<button type='submit' id='mulai_ujian' class='btn btn-success'>Mulai Ujian</button>";
												}else{
													echo "<button type='submit' id='mulai_ujian' class='btn btn-success' disabled>Mulai Ujian</button>";
												}
											?>
										</div>
									</div>
								</div>
								<div id ="soal_tampil"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div> <?php require_once "pages/copyright.php";?> </div>
	</div>
	<!-- and wrapper -->