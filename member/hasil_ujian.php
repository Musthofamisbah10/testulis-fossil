    <title>Manual Ujian - TesTulis FOSSIL</title>

    <!-- link  -->
	<link href='css/dataTables.bootstrap.min.css'/>
	<script src='js/jquery-1.12.4.js'></script>
	<script src='js/jquery.dataTables.min.js'></script>
	<script src='js/dataTables.bootstrap.min.js'></script>

    <!-- start wrapper -->

    <div class = "main-wrapper">  

    <!-- navbar -->
    <?php require_once "pages/navbar.php";?>
    <?php require_once "pages/sidebar.php";?>

    <div class = "page-wrapper">
        <div class = "container-fluid">
            <div class = "row page-titles">
                <div class = "col-md-6 col-8 align-self-center">
                    <h3 class = "text-themecolor m-b-0 m-t-0">Manual</h3>
                    <ol class = "breadcrumb">
                        <li class = "breadcrumb-item"><a href="home">Home</a></li>
                        <li class = "breadcrumb-item"><a href="histori">Histori Ujian</a></li>
                        <li class = "breadcrumb-item active">Manual</li>
                    </ol>
                </div>
            </div>
            <div class = "row">
                <div class = "col-sm-12">
                    <div class = "card">
                        <div class = "card-block">
                            <h4 class = "card-title">Manual</h4>
                            <table id="example" class="display table table-striped table-bordered" style="width:100%">
                                <thead>
                                <tr>
                                    <td>No</td>
                                    <td>Soal</td>
                                    <td>Hasil Stem</td>
                                    <td>Aksi</td>
                                </tr>
                                </thead>
                                <!-- action tabel -->
                                <tbody>
                                    <?php
                                        $no = 0;
                                        list ($dt, $dt1, $dt2) = get_soal_from_id();
                                        foreach($dt as $data) {
                                            echo "<tr>
                                            <td>".($no+1)."</td>
                                            <td>".($dt[$no])."</td>
                                            
                                            </tr>";
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>