<title>Ujian - TesTulis FOSSIL</title>

    <!-- start wrapper -->
    <div id="main-wrapper">
        
    <!-- header -->
    <?php require_once "pages/navbar.php";?>
    <?php require_once "pages/sidebar.php";?>
      
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-6 col-8 align-self-center">
                    <h3 class="text-themecolor m-b-0 m-t-0">Ujian</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="home">Home</a></li>
                        <li class="breadcrumb-item active">Ujian</li>
                    </ol>
                </div>
            </div>

        <!-- respon sistem -->
        <?php if(isset($_GET['sudah'])) { ?>
            <div class="alert alert-success">
                Jawaban ujian telah di simpan
            </div>
        <?php } ?>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-block">
                    <h4 class="card-title">Ujian</h4>
                    <div class="text-left"> 
                        <span class="text-muted">Ujian yang tersedia:</span>
                <!-- start tabel -->
                <table id="example" class="display table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Ujian</th>
                        <th>Materi</th>
                        <th>Pengurus</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
            
                <!-- action table -->
                <tbody>
                    <?php 
                        $no = 0;
                        list($dt, $dt1, $dt2, $dt3) = get_ujian_tulis();
                        foreach($dt as $data) {
                            echo "<tr>
                            <td>".($no+1)."</td>
                            <td>".$dt[$no]."</td>
                            <td>".get_materi_from_id($dt3[$no])['nama_materi']."</td>";
                            echo "
                            <td>".get_pengurus_from_id($dt1[$no])."</td>
                            <td>";

                            // cek status ujian
                            if(cek_status_ujian_member($_SESSION['idne'],$dt2[$no])) {
                                echo "<a href='index.php?p=do_ujian&iduji=".$dt2[$no]."'>Kerjakan</a>";
                            } else {
                                echo "Sudah dikerjakan";
                            }
                                echo "
                            </td>
                            </tr>";
                            $no++;
                        }
                    ?>
                    </tbody>
                </table>
                <!-- and table -->
                            </div>
                            </div>
                        </div>
                    </div>
                </div>       
            </div>
            <!-- footer -->
            <div> <?php require_once "pages/copyright.php";?> </div>          
        </div>       
    </div>
    <!-- and wrapper -->

    <script>
        $('#example').dataTable( {
        "searching": true
        } );
    </script>