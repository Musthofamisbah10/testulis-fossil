<div>

	<!-- fungsi rabin-karp -->
	<?php

		function rabin_manual($input,$kgram) {
			$data_pecah = pecah_kata(hapus_simbol($input));
			$hasil_case_folding = implode(' ',$data_pecah); 				//
			$hasil_tokenizing = explode(' ',$hasil_case_folding);			//
			for($i=0;$i<count($data_pecah);$i++){
		
				$result=mysql_query("SELECT kata FROM katahubung WHERE kata = '$data_pecah[$i]'");
				$jml=mysql_num_rows($result); 
			
					if($jml>0){
						$cek_filtering[$i] = '';
					}else{
						$cek_filtering[$i] = $data_pecah[$i];
					};
				}           
			
			$hasil_filtering = strtolower(implode(" ",$cek_filtering)); 	//
			$array_data_filtering = explode(" ",$hasil_filtering);
			$data_steam = array();
		
				foreach($array_data_filtering as $katasteaming) {	
					$data_steam[] = cari_kata_dasar($katasteaming);
				}

			$hasil_stemming = $data_steam;                     						//
			$x1= implode(' ',$data_steam);
			$data = hapus_spasi($x1);
			$ngrams = array();
			$hasil_ngram = array();
			$len = strlen($data);
			
			for($i = 0; $i < $len; $i++) {
				if($i > ($kgram - 2)) {
					$ng = '';
						for($j = $kgram-1; $j >= 0; $j--) {
							$ng .= $data[$i-$j]; 		
						}
						
						$hasil_ngram[] = $ng;								//
						//$ngrams[] = konversi_hash($ng);
							
						$charArray = str_split($ng);
						$hash = 0;
						$sum = 0;
						$result = array();
						$jml = count($charArray);
							
						foreach($charArray as $char){
							$jml--;
							$hash = ord($char);
							$z= ($hash * pow(10,$jml));
							
							//echo $z."+".$jml."  ";
							$result[] = "( ".$hash ." * 10^".$jml." )";
							$sum = $sum+$z;;   
						}
							
						//echo $sum;
						//$sum =$sum % 101;
						$print_hash[] = implode(' + ', $result) ." = ". $sum;				//
						//return $sum;
						$hasil_hashing[] = $sum;
					}
				}	 
			return array($hasil_case_folding, $hasil_tokenizing, $array_data_filtering, $hasil_stemming, $hasil_ngram, $hasil_hashing, $print_hash);
		}
	?>

	<!-- bar -->
	<?php require_once "pages/navbar.php";?>
    <?php require_once "pages/sidebar.php";?>

	<div class="page-wrapper">
		<div class="container-fluid">
			<div class="row page-titles">
				<div class="col-md-6 col-8 align-self-center">
					<h3 class ="text-themecolor m-bm0 m-t-0">Manual Rabin-Karp</h3>
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="home">Home</a></li>
						<li class ="breadcrumb-item active">Manual</li>
					</ol>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="card">
						<div class="card-block">
							<div class="col-sm-12 text-center"><h3>Manual Rabin-Karp</h3></div>
							<form action="" method="post">
								<div class="row" style='padding:0px;margin:0px; margin-bottom:-40px;'>
									<div class="col-sm-6">
										<div class="card">
											<div class="card-header" style="background-color: #636c72; color: #fff;">Jawaban</div>
											<div class="card-body">
												<div class="form-group">
													<textarea class="form-control" name="jawaban" rows="5" required id="comment"></textarea>
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="card">
											<div class="card-header" style="background-color: #636c72; color: #fff;">Kunci Jawaban</div>
											<div class="card-body">
												<div class="form-group">
													<textarea class="form-control" name="kunci" rows="5" required id="comment"></textarea>
												</div>
											</div>
										</div>
									</div>
								</div>
									<div class="col-sm-6">
										<div class="card">
											<div class="card-header" style="background-color: #636c72; color: #fff;">K-Gram</div>
											<div class="card-body">
												<div class="form-group">
													<input type="text" class="form-control" name="kgram" placeholder="Masukan Nilai K-gram">
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-6"></div>
									<div class="col-sm-12 text-left" style='padding-top:0;margin:0px; margin-top: -34px;'>
										<button id="buttonProses" type="submit" class="btn btn-primary">Proses</button>
									</div>
								</div>
							</form>
						</div>

						<!-- php -->
						<?php
							if(isset($_POST['jawaban'])){
								$kgram = $_POST['kgram'];
								$jawaban = $_POST['jawaban'];
								$kunci = $_POST['kunci'];

								list($hasil_case_folding, $hasil_tokenizing, $hasil_filtering, $hasil_stemming, $hasil_ngram, $hasil_hashing, $print_hash) = rabin_manual($jawaban,$kgram);
								list($hasil_case_folding1, $hasil_tokenizing1, $hasil_filtering1, $hasil_stemming1, $hasil_ngram1, $hasil_hashing1, $print_hash1) = rabin_manual($kunci,$kgram);
							}
						?>

						<div class="col-sm-12 margin">
							<hr>
							<div class="card" id="card-parent">
								<div class="card-block">
									<div class="row" style='padding:0px;margin:0px;'>
										<div class="col-sm-6">
											<div class="card">
											<div class="card-header" style="background-color: #636c72; color: #fff;">Jawaban</div>
												<div class="card-body">
													<div class="form-group">
														<textarea class="form-control" name="jawaban" rows="5" required id="comment" ><?php echo $jawaban ?></textarea>
													</div>
												</div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="card">
												<div class="card-header" style="background-color: #636c72; color: #fff;">Kunci Jawaban</div>
												<div class="card-body">
													<div class="form-group">
														<textarea class="form-control" name="kunci" rows="5" required id="comment" ><?php echo $kunci ?></textarea>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="row" style='padding:0px;margin:0px; margin-top:-65px;'>
										<div class="col-sm-12 text-center" ><h4>Case Folding dan Hapus Markup</h4></div>
									</div>
									<div class="row" style='padding:0px;margin:0px; margin-top:10px;'>
										<div class="col-sm-6">
											<div class="card">
												<div class="card-header" style="background-color: #636c72; color: #fff;">Jawaban</div>
												<div class="card-body">
													<div class="form-group">
														<textarea class="form-control" name="jawaban" rows="5" required id="comment"><?php echo $hasil_case_folding;?></textarea>
													</div>
												</div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="card">
												<div class="card-header" style="background-color: #636c72; color: #fff;">Kunci Jawaban</div>
												<div class="card-body">
													<div class="form-group">
														<textarea class="form-control" name="kunci" rows="5" required id="comment"><?php echo $hasil_case_folding1;?></textarea>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="row" style='padding:0px;margin:0px; margin-top:-47px;'>
											<div class="col-sm-12 text-center" style=''><h4>Tokenizing</h4></div>										
										</div>
										<div class="row" style='padding:0px;margin:-14px; margin-top:10px;'>
											<div class="col-sm-6">
												<div class="card">
													<div class="card-header" style="background-color: #636c72; color: #fff;">Jawaban</div>
													<div class="card-body">
														<div class="form-group">
															<textarea class="form-control" name="jawaban" rows="5" required id="comment"><?php foreach($hasil_tokenizing as $data_tokenizing) {echo $data_tokenizing."\n";}?></textarea>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="card">
													<div class="card-header" style="background-color: #636c72; color: #fff;">Kunci Jawaban</div>
													<div class="card-body">
														<div class="form-group">
															<textarea class="form-control" name="kunci" rows="5" required id="comment"><?php foreach($hasil_tokenizing1 as $data_tokenizing) {echo $data_tokenizing."\n";}?></textarea>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="row" style='padding:0px;margin:0px; margin-top:-47px;'>
											<div class="col-sm-12 text-center" style=''><h4>Filtering</h4></div>										
										</div>
										<div class="row" style='padding:0px;margin:-14px; margin-top:10px;'>
											<div class="col-sm-6">
												<div class="card">
													<div class="card-header" style="background-color: #636c72; color: #fff;">Jawaban</div>
													<div class="card-body">
														<div class="form-group">
															<textarea class="form-control" name="jawaban" rows="5" required id="comment">
																<?php
																	foreach($hasil_stemming as $data_stemming) {
																		if($data_stemming!=null){
																			echo $data_stemming."\n";
																			} else {}
																	}
																?>
															</textarea>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="card">
													<div class="card-header" style="background-color: #636c72; color: #fff;">Kunci Jawaban</div>
													<div class="card-body">
														<div class="form-group">
															<textarea class="form-control" name="kunci" rows="5" required id="comment">
																<?php
																	foreach($hasil_stemming1 as $data_stemming) {
																		if($data_stemming!=null){
																			echo $data_stemming."\n";
																			} else {}
																	}
																?>
															</textarea>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="row" style='padding:0px;margin:0px; margin-top:-47px;'>
											<div class="col-sm-12 text-center"><h4>Stemming</h4></div>										
										</div>
										<div class="row" style='padding:0px;margin:-14px; margin-top:10px;'>
											<div class="col-sm-6">
												<div class="card">
													<div class="card-header" style="background-color: #636c72; color: #fff;">Jawaban</div>
													<div class="card-body">
														<div class="form-group">
															<textarea class="form-control" name="jawaban" rows="5" required id="comment">
																<?php
																	foreach($hasil_stemming as $data_stemming) {
																		if($data_stemming!=null){
																			echo $data_stemming."\n";
																		} else {}
																	}
																?>
															</textarea>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="card">
													<div class="card-header" style="background-color: #636c72; color: #fff;">Kunci Jawaban</div>
													<div class="card-body">
														<div class="form-group">
															<textarea class="form-control" name="jawaban" rows="5" required id="comment">
																<?php
																	foreach($hasil_stemming1 as $data_stemming) {
																		if($data_stemming!=null){
																			echo $data_stemming."\n";
																		} else {}
																	}
																?>
															</textarea>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="row" style='padding:0px;margin:0px; margin-top:-47px;'>
											<div class="col-sm-12 text-center" style=''><h4>Penggabungan Data Hasil Stemming</h4></div>										
										</div>
										<div class="row" style='padding:0px;margin:-14px; margin-top:10px;'>
											<div class="col-sm-6">
												<div class="card">
													<div class="card-header" style="background-color: #636c72; color: #fff;">Jawaban</div>
													<div class="card-body">
														<div class="form-group">
															<textarea class="form-control" name="jawaban" rows="5" required id="comment">
																<?php 
																	foreach($hasil_stemming as $data_stemming) {
																		if($data_stemming!=null) {
																			echo $data_stemming;
																			} else {}
																	}
																?>
															</textarea>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="card">
													<div class="card-header" style="background-color: #636c72; color: #fff;">Kunci Jawaban</div>
													<div class="card-body">
														<div class="form-group">
															<textarea class="form-control" name="kunci" rows="5" required id="comment">
																	<?php 
																		foreach($hasil_stemming1 as $data_stemming) {
																			if($data_stemming!=null) {
																				echo $data_stemming;
																				} else {}
																		}
																	?>
															</textarea>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="row" style='padding:0px;margin:0px; margin-top:-47px;'>
											<div class="col-sm-12 text-center" style=''><h4>Parsing K-Gram</h4></div>										
										</div>
										<div class="row" style='padding:0px;margin:-14px; margin-top:10px;'>
											<div class="col-sm-6">
												<div class="card">
													<div class="card-header" style="background-color: #636c72; color: #fff;">Jawaban</div>
													<div class="card-body">
														<div class="form-group">
															<table class="table table-bordered">
																<thead>
																	<tr>
																		<th width="30%">No</th>
																		<th>K-Gram</th>
																	</tr>
																</thead>
																<tbody>
																<?php 
																		$no=1;foreach($hasil_ngram as $data_ngram){
																			if($data_ngram!=null){
																				echo "<tr>
																					<td>".$no."</td>
																					<td>".$data_ngram."</td>
																				</tr>";
																				$no++;
																			}else {}
																		}
																	?>
																</tbody>
															</table>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="card">
													<div class="card-header" style="background-color: #636c72; color: #fff;">Kunci Jawaban</div>
													<div class="card-body">
														<div class="form-group">
															<table class="table table-bordered">
																<thead>
																	<tr>
																		<th>No</th>
																		<th>K-Gram</th>
																	</tr>
																</thead>
																<tbody>
																	<?php 
																		$no=1;foreach($hasil_ngram1 as $data_ngram){
																			if($data_ngram!=null){
																				echo "<tr>
																					<td>".$no."</td>
																					<td>".$data_ngram."</td>
																				</tr>";
																				$no++;
																			}else {}
																		}
																	?>
																</tbody>
															</table>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="row" style='padding:0px;margin:0px; margin-top:-47px;'>
											<div class="col-sm-12 text-center" style=''><h4>Hashing K-Gram</h4></div>										
										</div>
										<div class="row" style='padding:0px;margin:-14px; margin-top:10px;'>
											<div class="col-sm-6">
												<div class="card">
													<div class="card-header" style="background-color: #636c72; color: #fff;">Jawaban</div>
													<div class="card-body">
														<div class="form-group">
															<table class="table table-bordered">
																<thead>
																	<tr>
																		<th>No</th>
																		<th>K-Gram</th>
																		<th>Hash</th>
																	</tr>
																</thead>
																<tbody>
																	<?php 
																		$no=1;foreach($print_hash as $data_hash){
																			if($data_hash!=null){
																				echo "<tr>
																					<td>".$no."</td>
																					<td>".$hasil_ngram[($no-1)]."</td>
																					<td>".$data_hash."</td>
																				</tr>";
																				$no++;
																			}else {}
																		}
																	?>
																</tbody>
															</table>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="card">
													<div class="card-header" style="background-color: #636c72; color: #fff;">Kunci Jawaban</div>
													<div class="card-body">
														<div class="form-group">
															<table class="table table-bordered">
																<thead>
																	<tr>
																		<th>No</th>
																		<th>K-Gram</th>
																		<th>Hash</th>
																	</tr>
																</thead>
																<tbody>
																	<?php 
																		$no=1;foreach($print_hash1 as $data_hash){
																			if($data_hash!=null){
																				echo "<tr>
																					<td>".$no."</td>
																					<td>".$hasil_ngram1[($no-1)]."</td>
																					<td>".$data_hash."</td>
																				</tr>";
																				$no++;
																			}else {}
																		}
																	?>
																</tbody>
															</table>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="row" style='padding:0px;margin:0px; margin-top:-47px;'>
											<div class="col-sm-12 text-center" style=''><h4>Mencari Hash Yang Cocok</h4></div>										
										</div>
										<div class="row" style='padding:0px;margin:-14px; margin-top:10px;'>
											<div class="col-sm-5">
												<div class="card">
													<div class="card-header" style="background-color: #636c72; color: #fff;">Jawaban</div>
													<div class="card-body">
														<div class="form-group">
															<table class="table table-bordered">
																<thead>
																	<tr>
																		<th>No</th>
																		<th>K-Gram</th>
																		<th>Hash</th>
																	</tr>
																</thead>
																<tbody>
																	<?php 
																		$cocok =array();
																		$hasil_hashing_tidak_cocok=array_diff($hasil_hashing,$hasil_hashing1);

																		$no=1;foreach($hasil_hashing_tidak_cocok as $data_hash){
																			if($data_hash!=null){
																				echo "<tr>
																					<td>".$no."</td>
																					<td>".$hasil_ngram[array_search($data_hash, $hasil_hashing)]."</td>
																					<td>".$data_hash."</td>
																				</tr>";
																				$cocok[] = "tidak";
																				$no++;
																			}else {}}

																			$hasil_hashing_cocok=array_intersect($hasil_hashing,$hasil_hashing1);
																			foreach($hasil_hashing_cocok as $data_hash){
																				if($data_hash!=null){
																					echo "<tr>
																						<td>".$no."</td>
																						<td>".$hasil_ngram[array_search($data_hash, $hasil_hashing)]."</td>
																						<td>".$data_hash."</td>
																					</tr>";
																					$cocok[] = "ya";
																					$no++;
																				} else {}}
																	?>
																</tbody>
															</table>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-5">
												<div class="card">
													<div class="card-header" style="background-color: #636c72; color: #fff;">Kunci Jawaban</div>
													<div class="card-body">
														<div class="form-group">
															<table class="table table-bordered">
																<thead>
																	<tr>
																		<th>No</th>
																		<th>K-Gram</th>
																		<th>Hash</th>
																	</tr>
																</thead>
																<tbody>
																	<?php 
																		$no=1;foreach($hasil_hashing1 as $data_hash){
																			if($data_hash!=null){
																				echo "<tr>
																					<td>".$no."</td>
																					<td>".$hasil_ngram1[($no-1)]."</td>
																					<td>".$data_hash."</td>
																				</tr>";
																				$no++;
																			}
																		}
																	?>
																</tbody>
															</table>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-2">
												<div class="card">
													<div class="card-header" style="background-color: #636c72; color: #fff;">Cocok</div>
													<div class="card-body">
														<div class="form-group">
															<table class="table table-bordered">
																<thead>
																	<tr>
																		<th>Kecocokan</th>
																	</tr>
																</thead>
																<tbody>
																	<?php 
																		$no=1; foreach($cocok as $print_cocok){
																			if($print_cocok!=null){
																				echo "<tr>
																					<td>".$print_cocok."</td>
																				</tr>";
																				$no++;
																			}else{}}
																	?>
																</tbody>
															</table>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="row" style='padding:0px;margin:0px; margin-top:-47px;'>
											<div class="col-sm-12 text-center" style=''><h4>Hasil Algoritma</h4></div>
										</div>
										<div class="row" style='padding:0px;margin:-14px; margin-top:10px;'>
											<div class="col-sm-12">
												<div class="card">
													<div class="card-header" style="background-color: #636c72; color: #fff;">Hasil Algoritma</div>
													<div class="card-body">
														<?php 
															$resultintersect = array_intersect($hasil_hashing, $hasil_hashing1);
															$resultintersect1 = array_intersect($hasil_hashing1, $hasil_hashing);
															
															$total1=count($resultintersect);
															$total2=count($resultintersect1);
															
															$jtotalarray1=count($hasil_ngram);
															$jtotalarray2=count($hasil_ngram1);
															
															$x=((2*$total1)/($jtotalarray1+$jtotalarray2)*100);
															echo dua_digit($x)." %"; 
														?>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!--  -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div> <?php require_once "pages/copyright.php";?> </div>
</div>