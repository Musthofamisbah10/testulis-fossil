<title>Histori Ujian - TesTulis FOSSIL</title>
	
    <!-- link  -->
	<link href='css/dataTables.bootstrap.min.css'/>
	<script src='js/jquery-1.12.4.js'></script>
	<script src='js/jquery.dataTables.min.js'></script>
	<script src='js/dataTables.bootstrap.min.js'></script>

    <!-- start wrapper -->
    <div id="main-wrapper">
        
    <!-- navbar -->
    <?php require_once "pages/navbar.php";?>
    <?php require_once "pages/sidebar.php";?>
      
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-6 col-8 align-self-center">
                    <h3 class="text-themecolor m-b-0 m-t-0">Histori Ujian</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="home">Home</a></li>
                        <li class="breadcrumb-item active">Histori Ujian</li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-block">
                            <h4 class="card-title">Histori Ujian</h4>
                            <!-- </h4><a href="hasil_ujian" class="btn pull-right hidden-sm-down btn-success">Hitungan Manual</a>    -->
                            <!-- start tble -->
                            <table id="example" class="display table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Ujian</th>
                                    <th>Nilai</th>
                                </tr>
                            </thead>
                            <!-- action histori ujian -->
                            <tbody>

                                <?php
                                    $no = 0;
                                    list($id_ujian, $nilai_ujian) = get_hasil_ujian_member($_SESSION['idne']);
                                      
                                    if($id_ujian != null) {
                                        foreach ($id_ujian as $ujian) {
                                        echo "<tr>
                                            <td>".($no+1)."</td>
                                            <td>".get_ujian_all($ujian)[0]."</td>
                                            <td>".$nilai_ujian[$no]."</td>
                                            </tr>";
                                        $no++;
                                        }
                                    }
                                ?>

                            </tbody>
                            </table>
                                <!-- and table --> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- footer -->
        </div> <?php require_once "pages/copyright.php";?> </div>
    </div>
    <!-- and wrapper -->

    <!-- validasi -->
    <script>
    $('#example').dataTable( {
        "searching": true
    } );
    
    function hapus(z) {
    
	var id = z;
	var url ='hapus_ujian';
	
	alertify.confirm('Peringatan', 'Yakin akan menghapus data?', 
    
    function(){
		$.post(url, {id_ujian: id},
        function() {		

				window.location = "data_ujian&sukses_delete=1";

		}); },
        
        function(){ }); 
				 
	}
   </script>