<div>

	<!-- bar -->
	<?php require_once "pages/navbar.php";?>
    <?php require_once "pages/sidebar.php";?>

	<div class="page-wrapper">
		<div class="container-fluid">
			<div class="row page-titles">
				<div class="col-md-6 col-8 align-self-center">
					<h3 class ="text-themecolor m-bm0 m-t-0">Manual</h3>
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="home">Home</a></li>
						<li class ="breadcrumb-item active">Manual</li>
					</ol>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="card">
						<div class="card-block">
							<div class="col-sm-12 text-center"><h3>Manual Rabin-Karp</h3></div>
							<form action="system" method="post">
								<div class="row" style='padding:0px;margin:0px; margin-bottom:-40px;'>
									<div class="col-sm-6">
										<div class="card">
											<div class="card-header" style="background-color: #636c72; color: #fff;">Jawaban</div>
											<div class="card-body">
												<div class="form-group">
													<textarea class="form-control" name="jawaban" rows="5" required id="comment"></textarea>
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="card">
											<div class="card-header" style="background-color: #636c72; color: #fff;">Kunci Jawaban</div>
											<div class="card-body">
												<div class="form-group">
													<textarea class="form-control" name="kunci" rows="5" required id="comment"></textarea>
												</div>
											</div>
										</div>
									</div>
								</div>
									<div class="col-sm-6">
										<div class="card">
											<div class="card-header" style="background-color: #636c72; color: #fff;">K-Gram</div>
											<div class="card-body">
												<div class="form-group">
													<input type="text" class="form-control" name="kgram" placeholder="Masukan Nilai K-gram">
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-6"></div>
									<div class="col-sm-12 text-left" style='padding-top:0;margin:0px; margin-top: -34px;'>
										<button id="buttonProses" type="submit" class="btn btn-primary">Proses</button>
									</div>
								</div>
							</form>
						</div>

						<!-- php -->
						<?php
							if(isset($_POST['jawaban'])){
								$kgram = $_POST['kgram'];
								$jawaban = $_POST['jawaban'];
								$kunci = $_POST['kunci'];

								list($hasil_case_folding, $hasil_tokenizing, $hasil_filtering, $hasil_stemming, $hasil_ngram, $hasil_hashing, $print_hash) = rabin_manual($jawaban,$kgram);
								list($hasil_case_folding1, $hasil_tokenizing1, $hasil_filtering1, $hasil_stemming1, $hasil_ngram1, $hasil_hashing1, $print_hash1) = rabin_manual($kunci,$kgram);
							}
						?>

						<div class="col-sm-12 margin">
							<hr>
					
									</div>
									<!--  -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div> <?php require_once "pages/copyright.php";?> </div>
</div>