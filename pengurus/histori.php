<title>Nilai Member - TesTulis FOSSIL</title>
	
    <!-- link -->
	<link href='css/dataTables.bootstrap.min.css'/>
	<script src='js/jquery-1.12.4.js'></script>
	<script src='js/jquery.dataTables.min.js'></script>
	<script src='js/dataTables.bootstrap.min.js'></script>

    <!-- start wrapper -->
    <div id="main-wrapper">
        
        <!-- navbar - sidebar -->
        <?php require_once "pages/navbar.php";?>
        <?php require_once "pages/sidebar.php";?>
      
        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Nilai Member</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="home">Home</a></li>
                            <li class="breadcrumb-item active">Nilai Member</li>
                        </ol>
                    </div>
                </div>
                <div class="row">
                    <!-- Column -->
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title">Data Nilai</h4>
                                <div class="text-left">	
                                    <!-- start table -->
                                    <table id="example" class="display table table-striped table-bordered" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama Member</th>
                                                <th>Nama Ujian</th>
                                                <th>Nilai</th> 
                                            </tr>
                                        </thead>

                                    <!-- action hostori -->
                                    <tbody>
            
                                        <?php
                                        $no = 0;
                                        list($id_ujian, $id_member, $nilai_ujian) = get_hasil_ujian($_SESSION['idne']);
                                        
                                        if($id_ujian != null) {
                                            foreach ($id_ujian as $ujian) {
                                            echo "<tr>
                                                    <td>".($no+1)."</td>
                                                    <td>".get_member_all($id_member[$no])[0]."</td>
                                                    <td>".get_ujian_all($ujian)[0]."</td>
                                                    <td>".$nilai_ujian[$no]."</td>
                                                </tr>";
                                                $no++;
                                            }
                                        }
                                        ?>  
                                        </tbody>
                                    </table>
                                <!-- and table -->
                            </div>
                        </div>
                    </div>
                 </div>
            <!-- colum -->
            </div>
            <!-- footer -->
        </div> <?php require_once "pages/copyright.php";?> </div>
    </div>
    <!-- and wrapper -->

    <!-- Validasi histori ujian -->
   <script>
        $('#example').dataTable( {
        "searching": true
    } );

   function hapus(z) {
    
	var id = z;
	var url ='hapus_ujian';
	
	alertify.confirm('Peringatan', 'Yakin akan menghapus data?',
    function(){
		$.post(url, {id_ujian: id},
        function() {
				
				window.location = "data_ujian&sukses_delete=1";
				
		}); }, 
        
        function(){ }); 
				 
	}
   </script>