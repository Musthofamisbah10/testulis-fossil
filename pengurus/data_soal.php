<title>Data Soal - TesTulis FOSSIL</title>

    <!-- get data ujian -->
	<?php 
		if(isset($_GET['idujian'])) {
			$id_ujian = $_GET['idujian'];
		}
		if(get_soal_from_ujian($id_ujian)!=null) {
			$d = 1;
		list($soal, $kunci, $id_soal) = get_soal_from_ujian($id_ujian);
		} else {
			$d = null;
		}
	?>
	
    <!-- link -->
	<link href='css/dataTables.bootstrap.min.css'/>
	<script src='js/jquery-1.12.4.js'></script>
	<script src='js/jquery.dataTables.min.js'></script>
	<script src='js/dataTables.bootstrap.min.js'></script>

	<!-- start wrapper -->
    <div id="main-wrapper">
        
        <!-- navbar - sidebar -->
        <?php require_once "pages/navbar.php";?>
        <?php require_once "pages/sidebar.php";?>
      
        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">
                        Kelola Soal <?php echo get_ujian_all($id_ujian)[0]; ?>
                        </h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="home">Home</a></li>
							<li class="breadcrumb-item active"><a href="data_ujian">Data Ujian</a></li>
                            <li class="breadcrumb-item active">Kelola Soal</li>
                            <li class="breadcrumb-item active">
							
                            <?php 
								echo get_ujian_all($id_ujian)[0];
							?>

							</li>
                        </ol>
                    </div>
                </div>

                    <!-- respon sistem -->
				    <?php if(isset($_GET['sukses'])) { ?>
                    <div class="alert alert-success">
                    <strong>Berhasil</strong> menambah soal
                    </div>
                    <?php } else if(isset($_GET['sukses_edit'])) { ?>
                    <div class="alert alert-success">
                    <strong>Berhasil</strong> mengubah soal
                    </div>
                    <?php }else if(isset($_GET['sukses_delete'])) {?>
                    <div class="alert alert-success">
                    <strong>Berhasil</strong> menghapus soal
                    </div>
                    <?php }?>

                <div class="row">
                    <!-- Column -->
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title">Data Soal <?php 
								echo '"'.get_ujian_all($id_ujian)[0].'"'; ?>
                                </h4><a href="tambah_soal&idujian=<?php echo $id_ujian?>" class="btn pull-right hidden-sm-down btn-success">Tambah Soal</a>
                                <div class="text-left">
                                <!-- start table -->
                                <table id="example" class="display table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                        <th>No</th>
                                            <th style='width:20%'>Soal</th>
                                            <th>Kunci Jawaban</th>
                                            <th style='width:17%'>Aksi</th>   
                                        </tr>
                                    </thead>
                                    <tbody>

                                    <!-- aksi data ujian -->
                                        <?php
                                        $no = 0;
                                            if($d!=null) {
                                        foreach ($soal as $soal) {
                                        echo "<tr>
                                                <td>".($no+1)."</td>
                                                <td>".$soal."</td>
                                                <td>".$kunci[$no]."</td>
                                                <td>
                                                <a href='edit_soal&id=".$id_soal[$no]."' class='btn pull-left hidden-sm-down btn-info'>Edit</a>
                                                
                                                <button id='hapus' style='margin-left:5px' onclick=hapus(".$id_soal[$no].") class='hapus btn pull-left hidden-sm-down btn-danger'>Hapus</button></td>
                                                
                                            </tr>";
                                            $no++;
                                        }
                                            }else {
                                                
                                            }
                                        ?>

                                    </tbody>
                                </table>
                            <!-- and table -->
                            </div>
                        </div>
                    </div>
                </div>
            <!-- Column -->
            </div>
            <!--footer  -->
        </div> <?php require_once "pages/copyright.php";?> </div>
    </div>
    <!-- and wrapper -->

    <!-- validasi -->
   <script>
   $('#example').dataTable( {
  "searching": true
    } );

   function hapus(z) {
    
	var id = z;
	var url ='hapus_soal';
	
	alertify.confirm('Peringatan', 'Yakin akan menghapus soal?', 
    function(){
		$.post(url, {id_soal: id},
        function() {
			
				window.location = "data_soal&idujian=<?php echo $id_ujian ?>&sukses_delete=1";
				
		}); }, 
        
        function(){ }); 
				 
	}
   </script>