<title>Edit Profil - TesTulis FOSSIL</title>

    <!-- get data pengurus -->
	<?php 
		if(isset($_SESSION['idne'])) {
			$id_pengurus = $_SESSION['idne'];
			$data_pengurus = get_pengurus_alldata_from_id($id_pengurus);
			
		}
	?>
	
    <!-- start wrapper -->
    <div id="main-wrapper">
       
        <!-- navbar - sidebar -->
        <?php require_once "pages/navbar.php";?>
        <?php require_once "pages/sidebar.php";?>
      
        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Edit Profil</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="home">Home</a></li>
                            <li class="breadcrumb-item active">Edit Profil</li>
							<li class="breadcrumb-item active"><?php echo $data_pengurus[0];?></li>
                        </ol>
                    </div>
                </div>

                    <!-- respon sistem -->
                    <?php if(isset($_GET['sukses'])) { ?>
                        <div class="alert alert-success">
                        <strong>Berhasil</strong> edit profil pengurus
                        </div>
                    <?php } ?>
                    
                <div class="row">
                   <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-block">
                                <center class="m-t-30"> <img src="assets/images/users/<?php echo $data_pengurus[4];?>" class="img-circle" width="150" height="150" />
                                    <h4 class="card-title m-t-10"><?php echo $data_pengurus[0];?></h4>
                                    <h6 class="card-subtitle">Pengurus FOSSIL</h6>
                                </center>
                            </div>
							
                        </div>

                        <!-- card block edit pengurus -->
						<div class="card">
                            <div class="card-block">
                                <!-- start form -->
								<form action='simpan_pengurus' method='post' enctype="multipart/form-data">
								<div class="form-group">
                                <label class="col-md-12">Ubah Foto <sup class='iform'>(kosongkan jika tidak ada perubahan)</sup></label>
                                    <div class="col-md-12">
                                        <input type="file" name="image" />
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>
					<div class="col-lg-8 col-xlg-9 col-md-7">
                        <div class="card">
                            <div class="card-block">
                                <div class="form-horizontal form-material">
                                    <div class="form-group">
                                        <label class="col-md-12">Username</label>
                                        <div class="col-md-12">
                                            <input name='uname' type="text" value='<?php echo $data_pengurus[1];?>' class="form-control form-control-line" readonly >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">Nama Pengurus</label>
                                        <div class="col-md-12">
                                            <input name='nama_pengurus' type="text" value='<?php echo $data_pengurus[0];?>' class="form-control form-control-line" >
                                        </div>
                                    </div>  
									 
                                    <div class="form-group">
                                        <label class="col-md-12 after">Password <sup class='iform'>(kosongkan jika tidak ada perubahan)</sup></label>
                                        <div class="col-md-12">
                                            <input name='pass' type="password" placeholder='Masukan password' class="form-control form-control-line">
                                            <input name='id_pengurus' type="text" value='<?php echo $data_pengurus[3];?>' style="display:none">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
											<input type='submit' name='simpan' value='Simpan Perubahan' class="btn btn-info" /> 
                                        </div> 
                                    </div>
									</div>
                                </form>
                                <!-- and form -->
                            </div>
                        </div>
                    </div>
                </div>
            <!-- footer -->
        </div> <?php require_once "pages/copyright.php";?> </div>
    </div>
  <!-- and wrapper -->