<title>Dashboard - TesTulis FOSSIL</title>

    <!-- start wrapper -->
    <div id="main-wrapper">
        
        <!-- sidebar - navbar -->
        <?php require_once "pages/navbar.php";?>
        <?php require_once "pages/sidebar.php";?>
      
        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="home">Home</a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div>
                </div>
                <div class="row">
                    <!-- Column -->
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title">Hai, Pengurus <?php echo get_nama_pengurus($uname) ?></h4>
                                <div class="text-left">
                                    <span class="text-muted">Selamat datang kembali di TesTulis FOSSIL</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
            </div>
        <div> <?php require_once "pages/copyright.php";?> </div>
    </div> 
    </div>
   <!-- and wrapper -->