<title>Tambah Ujian - TesTulis FOSSIL</title>
	
    <!-- start wrapper -->
    <div id="main-wrapper">
       
        <!-- navbar - sidebar -->
        <?php require_once "pages/navbar.php";?>
        <?php require_once "pages/sidebar.php";?>
      
        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Tambah Ujian</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="home">Home</a></li>
							<li class="breadcrumb-item active"><a href="data_ujian">Data Ujian</a></li>
							 <li class="breadcrumb-item active">Tambah Ujian</li>
                        </ol>
                    </div>
                </div>

                <!-- card block tambah ujian -->
                <div class="row">
                   <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-block">
                                <center class="m-t-30"> <img src="assets/images/ujian.png" class="img-circle" width="150" height="150" />
                                    <h4 class="card-title m-t-10">Tambah Ujian</h4>
                                    <h6 class="card-subtitle">Isi data disamping untuk menambahkan ujian</h6>
                                </center>
                            </div>
                        </div>
                    </div>
					<div class="col-lg-8 col-xlg-9 col-md-7">
                        <div class="card">
                            <div class="card-block">
                                <form action='simpan_ujian' method='post' class="form-horizontal form-material">
                                    <div class="form-group">
                                        <label class="col-md-12">Nama Ujian</label>
                                        <div class="col-md-12">
                                            <input name='nama_ujian' type="text" placeholder='Masukan nama ujian'  class="form-control form-control-line" required>
                                        </div>
                                    </div>
                                   <div class="form-group">
                                         <label class="col-md-12">Token Ujian</label>
										 <div class='col-sm-12' style='padding:0px'>
											<div class="col-sm-9" style='float:left'>
												<input id='token' name='token' type="text" placeholder='Generate Token' value='' class="form-control form-control-line" readonly required>
											</div>
											<div class="col-sm-3" style='float:left'>
												<a href='#' onclick='generate()' class='btn btn-primary'>Generate</a>
											</div>
										</div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
											<input type='submit' name='tambah' value='Simpan' class="btn btn-info" />
											<a href='data_ujian' class="btn btn-danger">Batal</a>
                                        </div> 
                                    </div>
                                </form>
                                <!-- and form -->
                            </div>
                        </div>
                    </div>
                </div>
            <!-- footer -->
        </div> <?php require_once "pages/copyright.php";?> </div>
    </div>
    <!-- and wrapper -->

    <!-- action klick after BTN Token -->
	<script>
    function makeid() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 6; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text.toUpperCase();
}

	function generate() {
		
		document.getElementById("token").value = makeid();
	}
	</script>