<title>Edit Ujian - TesTulis FOSSIL</title>

    <!-- get data ujian -->
	<?php 
		if(isset($_GET['id'])) {
			$id_ujian = $_GET['id'];
			$data_ujian = get_ujian_all($id_ujian);
			
		}
	?>
	
    <!-- satrt wrapper -->
    <div id="main-wrapper">
       
        <!-- navbar - sidebar -->
        <?php require_once "pages/navbar.php";?>
        <?php require_once "pages/sidebar.php";?>
      
        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Edit Ujian</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="home">Home</a></li>
                            <li class="breadcrumb-item active">Edit Ujian</li>
							 <li class="breadcrumb-item active"><?php echo $data_ujian[0];?></li>
                        </ol>
                    </div>
                </div>
              
                <div class="row">
                   <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-block">
                                <center class="m-t-30"> <img src="assets/images/ujian.png" class="img-circle" width="150" height="150" />
                                    <h4 class="card-title m-t-10">Ujian</h4>
                                    <h6 class="card-subtitle">Edit data Ujian TesTulis FOSSIL</h6>
                                </center>
                            </div>
                        </div>
                        <!-- start form -->
						<form action='simpan_ujian' method='post' enctype="multipart/form-data">
                    </div>

                    <!-- card block edit ujian -->
					<div class="col-lg-8 col-xlg-9 col-md-7">
                        <div class="card">
                            <div class="card-block">
                                <div class="form-horizontal form-material">
                                    <div class="form-group">
                                        <label class="col-md-12">Nama Ujian</label>
                                        <div class="col-md-12">
                                            <input name='nama_ujian' type="text" value='<?php echo $data_ujian[0];?>' class="form-control form-control-line" >
                                            <input style='display:none' name='id_ujian' type="text" value='<?php echo $id_ujian;?>' class="form-control form-control-line" >
                                        </div>
                                    </div>
									 <div class="form-group">
                                         <label class="col-md-12">Token Ujian</label>
										 <div class='col-sm-12' style='padding:0px'>
											<div class="col-sm-9" style='float:left'>
												<input id='token' name='token' type="text" value='<?php echo $data_ujian[3];?>' class="form-control form-control-line" readonly >
											</div>
											<div class="col-sm-3" style='float:left'>
												<a href='#' onclick='generate()' class='btn btn-primary'>Generate</a>
											</div>
										</div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12 after">Status</label>
										<div class="col-sm-12">
                                            <select name='status' class="form-control form-control-line">
                                                <?php 

													if($data_ujian[4]==1) {
														echo "<option value='1' selected>".status_ujian($data_ujian[4])."</option>";
														echo "<option value='0'>".status_ujian(0)."</option>";
													} else {
														echo "<option value='0' selected>".status_ujian(0)."</option>";
														echo "<option value='1'>".status_ujian(1)."</option>";
													}
													
											
												?>
                                            </select>
                                        </div>
                                    </div> 
                                    <div class="form-group">
                                        <div class="col-sm-12">
											<input type='submit' name='simpan' value='Simpan Perubahan' class="btn btn-info" />
											<a href='data_ujian' class="btn btn-danger">Batal</a>
                                        </div> 
                                    </div>
								</div>
                            </form>
                        <!-- and form -->
                        </div>
                    </div>
                </div>
            </div>
        <!-- footer -->
        </div> <?php require_once "pages/copyright.php";?> </div>
    </div>
    <!-- and wrapper -->
	
    <!-- action aftr klick BTN TOKEN -->
	<script>
	function makeid() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 6; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text.toUpperCase();
    
    }

	function generate() {
		
		document.getElementById("token").value = makeid();
	}
	</script>
  