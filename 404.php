<!DOCTYPE html>
<html lang="en">

<title>Halaman Tidak Ditemukan - TesTulis FOSSIL</title>

    <?php require_once "pages/head.php"; ?>

    <body class="fix-header card-no-border">
        <section id="wrapper" class="error-page">
            <div class="error-box">
                <div class="error-body text-center">
                    <h1>404</h1>
                    <h3 class="text-uppercase">Halaman tidak ditemukan !</h3>
                    <a href="home" class="btn btn-info btn-rounded waves-effect waves-light m-b-40">Kembali ke halaman awal</a> </div>
                <div><?php require_once "pages/copyright.php";?></div>
            </div>
        </section>
    </body>
</html>
