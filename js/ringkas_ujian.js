$(document).ready(function()
{
	// hides all DIVs with the CLASS container
	// and displays the one with the ID 'home' only
	$(".contan").css("display","none");
	$("#soal1").css("display","block");
	
	// makes the navigation work after all containers have bee hidden 
	showViaLink($("div#navig a"));
	
	
});

// shows a given element and hides all others
function showViaKeypress(element_id)
{
	$(".contan").css("display","none");
	// if multiple keys are pressed rapidly this will hide all but the last pressed key's div
	$(".contan").hide(1);
	$(element_id).fadeIn();
}

// shows proper DIV depending on link 'href'
function showViaLink(array)
{
	array.each(function(i)
	{	
		$(this).click(function()
		{
			var target = $(this).attr("href");
			$(".contan").css("display","none");
			$(target).fadeIn();
		});
	});
}