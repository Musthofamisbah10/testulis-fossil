<?php

unset($_SESSION['ses']);
unset($_SESSION['username']);
unset($_SESSION['rule']);

session_destroy();
header("Location:admin_login");
?>