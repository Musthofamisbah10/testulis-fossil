<title>Data Admin - TesTulis FOSSIL</title>
	
    <!-- link JS -->
	<link href='css/dataTables.bootstrap.min.css'/>
	<script src='js/jquery-1.12.4.js'></script>
	<script src='js/jquery.dataTables.min.js'></script>
	<script src='js/dataTables.bootstrap.min.js'></script>

    <!-- strat wrapper -->
    <div id="main-wrapper">
        
    <!-- header -->
    <?php require_once "pages/navbar.php";?>
    <?php require_once "pages/sidebar.php";?>
      
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-6 col-8 align-self-center">
                    <h3 class="text-themecolor m-b-0 m-t-0">Data Admin</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="home">Home</a></li>
                        <li class="breadcrumb-item active">Data Admin</li>
                    </ol>
                </div>
            </div>

            <!-- respon sistem -->
            <?php if(isset($_GET['sukses'])) {?>
				<div class="alert alert-success">
				  <strong>Berhasil</strong> menambah admin
				</div>
			<?php } else if(isset($_GET['sukses_edit'])) { ?>
				<div class="alert alert-success">
				  <strong>Berhasil</strong> mengubah data admin
				</div>
			<?php }else if(isset($_GET['sukses_delete'])) {?>
				<div class="alert alert-success">
				  <strong>Berhasil</strong> menghapus admin
				</div>
			<?php }?>

            <div class="row">
                <!-- Column -->
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title">Data Admin</h4><a href="tambah_admin" class="btn pull-right hidden-sm-down btn-success">Tambah Admin</a>
                                <div class="text-left">
                                
                                <!-- strat tabel -->
                                <table id="example" class="display table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Username</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>

                                <!-- action tabel -->
                                <tbody>
                    
                                    <?php
                                    $no = 0;
                                    list($dt,$dt1,$dt2) = get_admin_all();
                                    foreach ($dt as $data) {
                                    echo "<tr>
                                            <td>".($no+1)."</td>
                                            <td>".$data."</td>
                                            <td>".$dt1[$no]."</td>
                                            <td><a href='edit_admin&id=".$dt2[$no]."' class='btn pull-left hidden-sm-down btn-info'>Edit</a>
                                            <button id='hapus' style='margin-left:5px' onclick=hapus(".$dt2[$no].") class='hapus btn pull-left hidden-sm-down btn-danger'>Hapus</button></td>
                                            
                                        </tr>";
                                        $no++;
                                    }
                                    ?>
                                </tbody>
                            </table> <!-- and tabel -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>   
        </div> <?php require_once "pages/copyright.php";?> </div>
    </div>
    <!-- and wrapper -->

    <!-- validasi -->
   <script>
    $('#example').dataTable( {
    "searching": true
    } );
    
    function hapus(z) {
    
	var id = z;
	var url ='hapus_admin';
	
	alertify.confirm('Peringatan', 'Yakin akan menghapus data admin?',
    
    function(){
		$.post(url, {id_admin: id},
        
        function() {
				
				window.location = "data_admin&sukses_delete=1";
				
		}); }
                , function(){ }); 
				 
	}
   </script>