<title>Edit Member - TesTulis FOSSIL</title>

    <!-- get data member -->
	<?php 
		if(isset($_GET['id'])) {
			$id_member = $_GET['id'];
			$data_member = get_member_all($id_member);
		}
	?>
	
    <!-- start wrapper -->
    <div id="main-wrapper">
       
    <!-- header -->
    <?php require_once "pages/navbar.php";?>
    <?php require_once "pages/sidebar.php";?>
      
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-6 col-8 align-self-center">
                    <h3 class="text-themecolor m-b-0 m-t-0">Edit Member</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="home">Home</a></li>
                        <li class="breadcrumb-item active">Edit Member</li>
						<li class="breadcrumb-item active"><?php echo $data_member[0];?></li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-xlg-3 col-md-5">
                    <div class="card">
                        <div class="card-block">
                            <center class="m-t-30"> <img src="assets/images/users/<?php echo $data_member[3];?>" class="img-circle" width="150" height="150" />
                                <h4 class="card-title m-t-10"><?php echo $data_member[0];?></h4>
                                <h6 class="card-subtitle">Member FOSSIL</h6>
                            </center>
                        </div>
                    </div>

                    <!-- card block image -->
					<div class="card">
                        <div class="card-block"> 
                            <!-- start form -->
							<form action='simpan_member' method='post' enctype="multipart/form-data">
							<div class="form-group">
                            <label class="col-md-12">Ubah Foto <sup class='iform'>(kosongkan jika tidak ada perubahan)</sup></label>
                            <div class="col-md-12">
                                <input type="file" name="image" />
                            </div>
                        </div>
                    </div>	
                </div>
            </div>

            <!-- card block data member -->
			<div class="col-lg-8 col-xlg-9 col-md-7">
                <div class="card">
                    <div class="card-block">
                        <div class="form-horizontal form-material">
                            <div class="form-group">
                                <label class="col-md-12">Username</label>
                                    <div class="col-md-12">
                                        <input name='uname' type="text" value='<?php echo $data_member[1];?>' class="form-control form-control-line" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Nama Member</label>
                                    <div class="col-md-12">
                                        <input name='nama_member' type="text" value='<?php echo $data_member[0];?>' class="form-control form-control-line">
                                    </div>
                                </div>  
                                <div class="form-group">
                                    <label class="col-sm-12">Materi</label>
                                    <div class="col-sm-12">
                                        <select name='kelas' class="form-control form-control-line">
                                            <?php 
                                                            
                                                list($dt, $dt1) = get_kelas_all();
                                                $no = 0;
                                                foreach($dt as $data) {
                                                if($dt1[$no]==$data_member[2]) {
                                                    echo "<option value='".$dt1[$no]."' selected>".$data."</option>";
                                                } else {
                                                    echo "<option value='".$dt1[$no]."'>".$data."</option>";
                                                }
                                                    $no++;
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                    <div class="form-group">
                                        <label class="col-md-12 after">Password <sup class='iform'>(kosongkan jika tidak ada perubahan)</sup></label>
                                        <div class="col-md-12">
                                            <input name='pass' type="password" placeholder='Masukan password' class="form-control form-control-line">
                                            <input name='id_member' type="text" value='<?php echo $id_member;?>' style="display:none">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
											<input type='submit' name='simpan' value='Simpan Perubahan' class="btn btn-info" />
											<a href='data_member' class="btn btn-danger">Batal</a>
                                        </div> 
                                    </div>
								</div>
                            </form>
                            <!-- and form -->
                        </div>
                    </div>
                </div>
            </div>   
            <!-- footer -->
        </div> <?php require_once "pages/copyright.php";?> </div>       
    </div>
    <!-- and wrapper -->