<title>Data Materi - TesTulis FOSSIL</title>
	
	<!-- link JS -->
	<link href='css/dataTables.bootstrap.min.css'/>
	<script src='js/jquery-1.12.4.js'></script>
	<script src='js/jquery.dataTables.min.js'></script>
	<script src='js/dataTables.bootstrap.min.js'></script>

	<!-- start wrapper -->
    <div id="main-wrapper">
        
    <!-- header -->
    <?php require_once "pages/navbar.php";?>
    <?php require_once "pages/sidebar.php";?>
      
    <div class="page-wrapper">
        <div class="container-fluid">  
            <div class="row page-titles">
                <div class="col-md-6 col-8 align-self-center">
                    <h3 class="text-themecolor m-b-0 m-t-0">Data Materi</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="home">Home</a></li>
                        <li class="breadcrumb-item active">Data Materi</li>
                    </ol>
                </div>
            </div>

                <!-- respon sistem -->
				 <?php if(isset($_GET['sukses'])) {?>
					<div class="alert alert-success">
					  <strong>Berhasil</strong> menambah data materi
					</div>
				<?php } else if(isset($_GET['sukses_edit'])) { ?>
					<div class="alert alert-success">
					  <strong>Berhasil</strong> mengubah data materi
					</div>
				<?php }else if(isset($_GET['sukses_delete'])) {?>
					<div class="alert alert-success">
					  <strong>Berhasil</strong> menghapus data materi
					</div>
				<?php }?>

			<!-- start row -->
    		<div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-block">
                            <h4 class="card-title">Data Materi</h4>
                            <a href="#" class="btn pull-right hidden-sm-down btn-success" data-toggle="modal" data-target="#exampleModal">Tambah Materi</a>
                            <div class="text-left">

								<!-- strat tabel -->
								<table id="example" class="display table table-striped table-bordered" style="width:100%">
									<thead>
										<tr>
											<th>No</th>
											<th>Nama Materi</th>
											<th>Aksi</th>
										</tr>
									</thead>

									<!-- action tabel -->
									<tbody>
										<?php
										$no = 0;
										list($dt,$dt1) = get_study_all(0);
										foreach ($dt as $data) {
										echo "<tr>
												<td>".($no+1)."</td>
												<td id='dat".($dt1[$no])."'>".$data."</td>
												<td><a href='#' class='btn pull-left hidden-sm-down btn-info' data-toggle='modal' data-target='#editmodal' onclick=edit(".$dt1[$no].")>Edit</a>
												<button id='hapus' style='margin-left:5px' onclick=hapus(".$dt1[$no].") class='hapus btn pull-left hidden-sm-down btn-danger'>Hapus</button></td>
											</tr>";
											$no++;
										}
										?>
									</tbody>
								</table>  
								<!-- and tabel -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- footer -->
        </div> <?php require_once "pages/copyright.php";?> </div>
    </div>
    <!-- and wrapper -->

	<!-- modal tambah materi -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Tambah Materi</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  		<span aria-hidden="true">&times;</span>
					</button>
		  		</div>
	
				<div class="modal-body">
				<!-- start form -->
					<form action='simpan_materi' method='post' class="form-horizontal">
						<div class="form-group">
							<label for="namakelass" class="col-md-12">Nama Materi :</label>
							<div class="col-md-12">
								<input name='nama_materi' placeholder='Masukan nama materi' type="text" class="form-control form-control-line" required>
							</div>
						</div>	
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
						<input type="submit" name='tambah' value='Simpan' class="btn btn-info"/>
					</div>
				</form>
				<!-- and form -->
			</div>
		</div>
	</div>
	
	<!-- modal Edit materi -->
	<div class="modal fade" id="editmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Edit Materi</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  		<span aria-hidden="true">&times;</span>
					</button>
		  		</div>
		  
				<div class="modal-body">
					<form action='simpan_materi' method='post' class="form-horizontal">
						<div class="form-group">
							<label for="namakelass" class="col-md-12">Nama Materi :</label>
							<div class="col-md-12">
								<input id='namapel' name='nama_materi' placeholder='Masukan nama materi' type="text" class="form-control form-control-line" required>
								<input id='idpel' name='id_materi' placeholder='Masukan nama kelas' type="text" class="form-control form-control-line" style='display:none' required>
							</div>
						</div>  
					</div>
		  
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
						<input type="submit" name='simpan' value='Simpan' class="btn btn-info"/>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	<!-- validasi -->
	<script>
	$('#example').dataTable( {
   	"searching": true
	} );

   	function hapus(z) { 
	var id = z;
	var url ='hapus_materi';
	
	alertify.confirm('Peringatan', 'Yakin akan menghapus data materi?', 
	function(){
		$.post(url, {id_materi: id}, 
		function() {
	
			window.location = "data_materi&sukses_delete=1";	

		}); },

		function(){ }); 		 
	}
	
	 function edit(z) {
		var id = z;
		var nama = document.getElementById("dat"+z).innerText;
		
		document.getElementById("namapel").value = nama;
		document.getElementById("idpel").value = id;
	}
   </script>