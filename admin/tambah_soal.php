<title>Tambah Soal - TesTulis FOSSIL</title>
	
    <!-- start wrapper -->
    <div id="main-wrapper">
       
    <!-- header -->
    <?php require_once "pages/navbar.php";?>
    <?php require_once "pages/sidebar.php";?>
      
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-6 col-8 align-self-center">
                    <h3 class="text-themecolor m-b-0 m-t-0">Tambah Soal</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="home">Home</a></li>
                        <li class="breadcrumb-item active"><a href="data_ujian">Data Ujian</a></li>
						<li class="breadcrumb-item active"><a href="data_soal&idujian=<?php echo $_GET['idujian']?>"><?php echo get_ujian_all($_GET['idujian'])[0]?></a></li>
						<li class="breadcrumb-item active">Tambah Soal</li>
                    </ol>
                </div>
            </div>
            <div class="row">
               <div class="col-lg-4 col-xlg-3 col-md-5">
                    <div class="card">
                        <div class="card-block">
                            <center class="m-t-30"> <img src="assets/images/soal.png" class="img-circle" width="150" width="height" />
                                <h4 class="card-title m-t-10">Tambah Soal</h4>
                                <h6 class="card-subtitle">Isi data disamping untuk menambahkan soal</h6>              
                            </center>
                        </div>
                    </div>
                </div>

                <!-- card block tambah soal -->
				<div class="col-lg-8 col-xlg-9 col-md-7">
                    <div class="card">
                        <div class="card-block">
                            <!-- start form -->
                            <form action='simpan_soal' method='post' class="form-horizontal form-material">
                                <div class="form-group">
                                    <label class="col-md-12">Soal Ujian</label>
                                    <div class="col-md-12">
                                       <textarea style='border:1px solid #cce3f1' name='soal' rows="5" class="form-control form-control-line" required></textarea>
                                    </div>
                                </div> 
								<div class="form-group">
                                    <label class="col-md-12">Kunci Jawaban</label>
                                    <div class="col-md-12">
                                       <textarea style='border:1px solid #cce3f1' name='kunci' rows="5" class="form-control form-control-line" required></textarea>
                                    </div>
                                </div>
									<input value='<?php echo $_GET['idujian']?>'style='display:none' name='id_ujian' type="text" placeholder='Masukan username'  class="form-control form-control-line" required>
                                <div class="form-group">
                                    <div class="col-sm-12">    
										<input type='submit' name='tambah' value='Simpan' class="btn btn-info" />
										<a href='data_soal&idujian=<?php echo $_GET['idujian']?>' class="btn btn-danger">Batal</a>
                                    </div> 										
                                </div>
                            </form>
                            <!-- and form -->
                        </div>
                    </div>
                </div>
            </div>
        <!-- footer -->
        </div> <?php require_once "pages/copyright.php";?> </div>
    </div>
    <!-- and wrapper -->