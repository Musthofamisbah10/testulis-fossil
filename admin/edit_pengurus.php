<title>Edit Pengurus - TesTulis FOSSIL</title>

    <!-- get data pengurus -->
	<?php 
		if(isset($_GET['id'])) {
			$id_pengurus = $_GET['id'];
			$data_pengurus = get_pengurus_alldata_from_id($id_pengurus);
			
		}
	?>
	
    <!-- start warpper -->
    <div id="main-wrapper">
       
    <!-- header -->
    <?php require_once "pages/navbar.php";?>
    <?php require_once "pages/sidebar.php";?>
        
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-6 col-8 align-self-center">
                    <h3 class="text-themecolor m-b-0 m-t-0">Edit Pengurus</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="home">Home</a></li>
                        <li class="breadcrumb-item active">Edit Pengurus</li>
						<li class="breadcrumb-item active"><?php echo $data_pengurus[0];?></li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-xlg-3 col-md-5">
                    <div class="card">
                        <div class="card-block">
                            <center class="m-t-30"> <img src="assets/images/users/<?php echo $data_pengurus[4];?>" class="img-circle" width="150" height='150px' />
                                <h4 class="card-title m-t-10"><?php echo $data_pengurus[0];?></h4>
                                <h6 class="card-subtitle">Pengurus FOSSIL</h6>
                            </center>
                        </div>
                    </div>

                    <!-- card block image -->
					<div class="card">
                        <div class="card-block">
                            <!-- start form -->
							<form action='simpan_pengurus' method='post' enctype="multipart/form-data">
							<div class="form-group">
                                <label class="col-md-12">Ubah Foto <sup class='iform'>(kosongkan jika tidak ada perubahan)</sup></label>
                            <div class="col-md-12">
                                <input type="file" name="image" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- card block data pengurus -->
			<div class="col-lg-8 col-xlg-9 col-md-7">
                <div class="card">
                    <div class="card-block">
                        <div class="form-horizontal form-material">
                            <div class="form-group">
                            <label class="col-md-12">Username</label>
                                <div class="col-md-12">
                                    <input name='uname' type="text" value='<?php echo $data_pengurus[1];?>' class="form-control form-control-line" >
                                </div>
                            </div>
                            <div class="form-group">
                            <label for="example-email" class="col-md-12">Nama Pengurus</label>
                                <div class="col-md-12">
                                    <input name='nama_pengurus' type="text" value='<?php echo $data_pengurus[0];?>' class="form-control form-control-line" >
                                </div>
                            </div>  
							<div class="form-group">
                            <label class="col-sm-12">Materi</label>
                                <div class="col-sm-12">
                                    <select name='materi' class="form-control form-control-line">

                                        <?php 
											list($id_matr, $nama_pel) = get_materi_all();
											$no = 0;
											foreach ($id_matr as $id_matr) {
												if($id_matr==$data_pengurus[2]) {
													echo "<option value='".$id_matr."' selected>".$nama_pel[$no]."</option>";
												} else {
													echo "<option value='".$id_matr."'>".$nama_pel[$no]."</option>";
												}
												$no++;
											}
										?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-12 after">Password <sup class='iform'>(kosongkan jika tidak ada perubahan)</sup></label>
                                <div class="col-md-12">
                                    <input name='pass' type="password" placeholder='Masukan password' class="form-control form-control-line">
                                    <input name='id_pengurus' type="text" value='<?php echo $data_pengurus[3];?>' style="display:none">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
									<input type='submit' name='simpan' value='Simpan Perubahan' class="btn btn-info" />
									<a href='data_pengurus' class="btn btn-danger">Batal</a>
                                </div> 
                            </div>
						    </div>
                        </form> <!-- and form -->
                    </div>
                </div>
            </div>
        </div>
        <!-- footer -->
        </div> <?php require_once "pages/copyright.php";?> </div>
    </div>
    <!-- and wrapper -->