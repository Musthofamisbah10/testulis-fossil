<div>

	<!-- bar -->
	<?php require_once "pages/navbar.php";?>
    <?php require_once "pages/sidebar.php";?>

	<div class="page-wrapper">
		<div class="container-fluid">
			<div class="row page-titles">
				<div class="col-md-6 col-8 align-self-center">
					<h3 class ="text-themecolor m-bm0 m-t-0">Akurasi Algoritma</h3>
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="home">Home</a></li>
						<li class ="breadcrumb-item active">Akurasi Algoritma</li>
					</ol>
				</div>
			</div>
			<nav>
			  	<ul class="nav nav-pills mb-3" id="myTab" id="pills-tab" role="tablist">
				 	<li class="nav-item">
				    	<a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Home</a>
				 	</li>
				 	<li class="nav-item">
				    	<a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Profile</a>
				 	</li>
				  	<li class="nav-item">
				    	<a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Contact</a>
				  </li>
				</ul>
				<div class="tab-content" id="pills-tabContent">
					<div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
						<div class="row">
							<div class="col-sm-12">
								<div class="card">
									<div class="card-block">
										<div class="col-sm-12 text-center"><h3>Akurasi Algoritma</h3></div>
										<table class="display table table-striped table-bordered" style="width:100% border-color:black;">
											<tr>
												<td>No</td>
												<td>Jawaban</td>
												<td>Kunci Jawaban</td>
												<td>Presentasi</td>
												<td>Tingkat Akurasi</td>
											</tr>
										</table>
										<tbody>
											<?php
												$no = 0;
												
											?>
										</tbody>
									</div>
									<!--  -->
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
						...
					</div>
					<div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
						...
					</div>
				</div>
				<!-- AND ROW -->
			</div>
		</div>
	<div> <?php require_once "pages/copyright.php";?> </div>
</div>