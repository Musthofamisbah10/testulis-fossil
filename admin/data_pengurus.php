<title>Data Pengurus - TesTulis FOSSIL</title>

    <!-- link JS -->
	<link href='css/dataTables.bootstrap.min.css'/>
	<script src='js/jquery-1.12.4.js'></script>
	<script src='js/jquery.dataTables.min.js'></script>
	<script src='js/dataTables.bootstrap.min.js'></script>

    <!-- start wrapper -->
    <div id="main-wrapper">
        
    <!-- bar -->
    <?php require_once "pages/navbar.php";?>
    <?php require_once "pages/sidebar.php";?>

    <!-- strat wrapper -->
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-6 col-8 align-self-center">
                    <h3 class="text-themecolor m-b-0 m-t-0">Data Pengurus</h3>
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="home">Home</a></li>
                        <li class="breadcrumb-item active">Data Pengurus</li>
                    </ol>
                </div>
            </div>

            <!-- respon sistem -->
			<?php if(isset($_GET['sukses'])) { ?>
				<div class="alert alert-success">
				  <strong>Berhasil</strong> menambah data pengurus
				</div>
			<?php } else if(isset($_GET['sukses_edit'])) { ?>
				<div class="alert alert-success">
				  <strong>Berhasil</strong> mengubah data pengurus
				</div>
			<?php }else if(isset($_GET['sukses_delete'])) {?>
				<div class="alert alert-success">
				  <strong>Berhasil</strong> menghapus data pengurus
				</div>
			<?php }?>
				
            <!-- row -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-block">
                            <h4 class="card-title">Data Pengurus</h4><a href="tambah_pengurus" class="btn pull-right hidden-sm-down btn-success">Tambah Pengurus</a>
                            <div class="text-left">

                                <!-- start tabel -->
                                <table id="example" class="display table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Username</th>
                                            <th>Materi</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>

                                    <!-- action tabel -->
                                    <tbody>
                                    <?php
                                        $no = 0;
                                        list($dt, $dt1, $dt2, $dt3) = get_pengurus_all();
                                        foreach ($dt as $data) {
                                        echo "<tr>
                                                <td>".($no+1)."</td>
                                                <td>".$data."</td>
                                                <td>".$dt1[$no]."</td>
                                                <td>".get_materi_from_id($dt2[$no])['nama_materi']."</td>
                                                <td><a href='edit_pengurus&id=".$dt3[$no]."' class='btn pull-left hidden-sm-down btn-info'>Edit</a>
                                                <button id='hapus' style='margin-left:5px' onclick=hapus(".$dt3[$no].") class='hapus btn pull-left hidden-sm-down btn-danger'>Hapus</button></td>
                                            </tr>";
                                            $no++;
                                        }
                                    ?>
                                </tbody>
                            </table>
                            </div> 
                        </div>
                    </div>
                </div>
            </div><!-- and row -->
            <!-- footer -->
        </div> <?php require_once "pages/copyright.php";?> </div>    
    </div>
    <!-- and wrapper -->

    <!-- validasi -->
    <script>
    $('#example').dataTable( {
        "searching": true
        });
        
    function hapus(z) { 
	var id = z;
	var url ='hapus_pengurus';
	alertify.confirm('Peringatan', 'Yakin akan menghapus data pengurus?',
    function(){
		$.post(url, {id_pengurus: id},
        function() {
            window.location = "data_pengurus&sukses_delete=1";
        });
    },
        function(){ 
        }); 			 
    }
    </script>