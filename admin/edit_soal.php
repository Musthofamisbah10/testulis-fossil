<title>Edit Soal - TesTulis FOSSIL</title>
	
    <!-- get data soal -->
    <?php 
		if(isset($_GET['id'])) {
			$id_soal = $_GET['id'];
			$data_soal = get_soal_from_id($id_soal);	
		}
	?>
	
    <!-- start warpper -->
    <div id="main-wrapper">
        
    <!-- header -->
    <?php require_once "pages/navbar.php";?>
    <?php require_once "pages/sidebar.php";?>
      
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-6 col-8 align-self-center">
                    <h3 class="text-themecolor m-b-0 m-t-0">Edit Soal</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="home">Home</a></li>
						<li class="breadcrumb-item active"><a href="data_ujian">Data Ujian</a></li>
						<li class="breadcrumb-item active"><a href="data_soal&idujian=<?php echo $data_soal[2]?>"><?php echo get_ujian_all($data_soal[2])[0]?></a></li>
                        <li class="breadcrumb-item active">Edit Soal</li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <!-- card block header data soal -->
                <div class="col-lg-4 col-xlg-3 col-md-5">
                    <div class="card">
                        <div class="card-block">
                            <center class="m-t-30"> <img src="assets/images/soal.png" class="img-circle" width="150" height="150" />
                                <h4 class="card-title m-t-10">Edit Soal</h4>
                                <h6 class="card-subtitle">Edit data Soal TesTulis</h6>
                            </center>
                        </div>
                    </div>
					<div class="card">
                        <div class="card-block">
                            <!-- start form -->
							<form action='simpan_soal' method='post' enctype="multipart/form-data">
                        </div>
                    </div>
                </div>

                <!-- card block data soal -->
				<div class="col-lg-8 col-xlg-9 col-md-7">
                    <div class="card">
                        <div class="card-block">
                            <div class="form-horizontal form-material">
                                <div class="form-group">
                                    <label class="col-md-12">Soal Ujian</label>
                                    <div class="col-md-12">
                                       <textarea style='border:1px solid #cce3f1' name='soal' rows="5" class="form-control form-control-line" required><?php echo $data_soal[0]?></textarea>
                                    </div>
                                </div> 
								<div class="form-group">
                                    <label class="col-md-12">Kunci Jawaban</label>
                                    <div class="col-md-12">
                                       <textarea style='border:1px solid #cce3f1' name='kunci' rows="5" class="form-control form-control-line" required><?php echo $data_soal[1]?></textarea>
                                    </div>
                                </div>
                                    <input value='<?php echo $id_soal?>'style='display:none' name='id_soal' type="text" class="form-control form-control-line" required>
								    <input value='<?php echo $data_soal[2]?>'style='display:none' name='id_ujian' type="text" class="form-control form-control-line" required>
                                <div class="form-group">
                                    <div class="col-sm-12">
										<input type='submit' name='simpan' value='Simpan Perubahan' class="btn btn-info" />
										<a href='data_soal&idujian=<?php echo $data_soal[2]?>' class="btn btn-danger">Batal</a>
                                    </div> 
                                </div>
							</div>
                            </form>
                            <!-- and form -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- footer -->
        </div> <?php require_once "pages/copyright.php";?> </div>
    </div>
	<!-- and wrapper -->