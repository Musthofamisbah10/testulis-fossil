<title>Tambah Pengurus - TesTulis FOSSIL</title>
	
    <!-- start warpper -->
    <div id="main-wrapper">
       
    <!-- header -->
    <?php require_once "pages/navbar.php";?>
    <?php require_once "pages/sidebar.php";?>
      
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-6 col-8 align-self-center">
                    <h3 class="text-themecolor m-b-0 m-t-0">Tambah Pengurus</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="home">Home</a></li>
						<li class="breadcrumb-item"><a href="data_pengurus">Data Pengurus</a></li>
						<li class="breadcrumb-item">Tambah Pengurus</li>
                    </ol>
                </div>  
            </div>

            <!-- respon sistem -->
            <?php if(isset($_GET['error'])) { ?>
				<div class="alert alert-danger">    
			        <strong>Menambah Pengurus Gagal!</strong> Username sudah digunakan
			    </div>
			<?php } ?>

            <div class="row">
                <div class="col-lg-4 col-xlg-3 col-md-5">
                    <div class="card">
                        <div class="card-block">
                            <center class="m-t-30"> <img src="assets/images/guru.png" class="img-circle" width="150" />
                                <h4 class="card-title m-t-10">Tambah Pengurus</h4>
                                <h6 class="card-subtitle">Isi data disamping untuk menambahkan Pengurus</h6> 
                            </center>
                        </div>
                    </div>
                </div>

                <!-- card block tambah pengurus -->
				<div class="col-lg-8 col-xlg-9 col-md-7">
                    <div class="card">
                        <div class="card-block">
                            <form action='simpan_pengurus' method='post' class="form-horizontal form-material">
                                <div class="form-group">
                                    <label class="col-md-12">Username</label>
                                    <div class="col-md-12">
                                        <input name='uname' type="text" placeholder='Masukan username'  class="form-control form-control-line" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="example-email" class="col-md-12">Nama Pengurus</label>
                                    <div class="col-md-12">
                                        <input name='nama_pengurus' placeholder='Masukan nama pengurus' type="text" class="form-control form-control-line" name="example-email" id="example-email" required>
                                    </div>
                                </div>  
								<div class="form-group">
                                    <label class="col-sm-12">Materi</label>
                                    <div class="col-sm-12">
                                        <select name='materi' class="form-control form-control-line" required>
										
											<?php 
												list($id_pel,$nama_pel) = get_materi_all();
												$no = 0;
												foreach ($id_pel as $id_pel) {
													echo "<option value='".$id_pel."'>".$nama_pel[$no]."</option>";
													$no++;
												}
											?>
                                               
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12 after">Password </label>
                                    <div class="col-md-12">
                                        <input name='pass' type="password" placeholder='Masukan password' class="form-control form-control-line" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">  
										<input type='submit' name='tambah' value='Simpan' class="btn btn-info" />
										<a href='data_pengurus' class="btn btn-danger">Batal</a>
                                    </div> 
                                </div>
                            </form>
                            <!-- and form -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- footer -->
        </div> <?php require_once "pages/copyright.php";?> </div>
    </div>
    <!-- and wrapper -->