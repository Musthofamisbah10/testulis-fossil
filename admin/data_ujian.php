<title>Data Ujian - TesTulis FOSSIL</title>
    
    <!-- link JS -->
	<link href='css/dataTables.bootstrap.min.css'/>
	<script src='js/jquery-1.12.4.js'></script>
	<script src='js/jquery.dataTables.min.js'></script>
	<script src='js/dataTables.bootstrap.min.js'></script>

	<!-- start wrapper -->
    <div id="main-wrapper">
    
    <!-- header -->
    <?php require_once "pages/navbar.php";?>
    <?php require_once "pages/sidebar.php";?>
      
    <div class="page-wrapper">
        <div class="container-fluid">    
            <div class="row page-titles">
                <div class="col-md-6 col-8 align-self-center">
                    <h3 class="text-themecolor m-b-0 m-t-0">Data Ujian</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="home">Home</a></li>
                        <li class="breadcrumb-item active">Data Ujian</li>
                    </ol>
                </div>
            </div>

            <!-- respon sistem -->
            <?php if(isset($_GET['sukses'])) { ?>
				<div class="alert alert-success">
				  <strong>Berhasil</strong> menambah data ujian
				</div>
			<?php } else if(isset($_GET['sukses_edit'])) { ?>
				<div class="alert alert-success">
				  <strong>Berhasil</strong> mengubah data ujian
				</div>
			<?php }else if(isset($_GET['sukses_delete'])) {?>
				<div class="alert alert-success">
				  <strong>Berhasil</strong> menghapus data ujian
				</div>
            <?php }?>
                
            <!-- start row -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-block">
                            <h4 class="card-title">Data Ujian</h4><a href="tambah_ujian" class="btn pull-right hidden-sm-down btn-success">Tambah Ujian</a>
                            <div class="text-left">	
                                <!-- start tabel -->
                                <table id="example" class="display table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Ujian</th>
                                            <th>materi</th>
                                            <th>Pembuat</th>
                                            <th>Kode Ujian</th>
                                            <th>Aktif</th>
                                            <th width="25%">Aksi</th>
                                        </tr>
                                    </thead>
                                                    
                                    <!-- action tabel -->
                                    <tbody>         
                                        <?php
                                        $no = 0;
                                        list($nm_ujian, $id_materi, $id_pengurus, $token, $status, $id_ujian) = get_ujian_all(0);
                                        foreach ($nm_ujian as $nama_ujian) {
                                        echo "<tr>
                                                <td>".($no+1)."</td>
                                                <td>".$nama_ujian."</td>
                                                <td>".get_materi_from_id($id_materi[$no])['nama_materi']."</td>
                                                <td>".get_pengurus_from_id($id_pengurus[$no])."</td>
                                                <td>".$token[$no]."</td>
                                                <td>".status_ujian($status[$no])."</td>
                                                <td><a href='edit_ujian&id=".$id_ujian[$no]."' class='btn pull-left hidden-sm-down btn-info'>Edit</a>
                                                <a style='margin-left:5px' href='data_soal&idujian=".$id_ujian[$no]."' class='btn pull-left hidden-sm-down btn-primary'>Kelola Soal</a>
                                                <button id='hapus' style='margin-left:5px' onclick=hapus(".$id_ujian[$no].") class='hapus btn pull-left hidden-sm-down btn-danger'>Hapus</button></td>
                                                
                                            </tr>";
                                            $no++;
                                        }
                                        ?>
                                    </tbody>
                                </table> <!-- and tabel -->
                             </div>    
                        </div>
                    </div>
                </div>
            </div> <!-- and row -->
        <!-- footer -->
        </div> <?php require_once "pages/copyright.php";?> </div> 
    </div>
    <!-- and wrapper -->

        <!-- validasi -->
   <script>
    $('#example').dataTable( {
    "searching": true
    } );

    function hapus(z) {
	var id = z;
	var url ='hapus_ujian';
	
	alertify.confirm('Peringatan', 'Yakin akan menghapus data ujian?', 
    function(){
		$.post(url, {id_ujian: id}, 
        function() {
				
				window.location = "data_ujian&sukses_delete=1";
				
		}); }
                , function(){ }); 
				 
	}
    </script>