<!DOCTYPE html>
<html lang="en">

<head>
	
	<?php 
	$pages_dir = "login";
	
	session_start();
	require_once "koneksi.php";
	require_once "pages/enkripsi.php";
	require_once "pages/fungsi.php";
	
	if(isset($_SESSION['rule'])) {
		if(enkrip("member")==$_SESSION['rule']) {
			$pages_dir = "member";
		} else if(enkrip("pengurus")==$_SESSION['rule']) {
			$pages_dir = "pengurus";
		}if(enkrip("admin")==$_SESSION['rule']) {
			$pages_dir = "admin";
		}
		$uname = $_SESSION['usname'];
		$idne = $_SESSION['idne'];
	}

	if(isset($_SESSION['usname'])) {
		require_once "pages/head.php";
	}else{
		require_once "pages/head_login.php";
	}
	?>
	
</head>
<body class="fix-header fix-sidebar card-no-border" style='margin:0px'>
   
    <?php
		
	if(!empty($_GET['p'])){
		$pages = scandir($pages_dir, 0);
		unset($pages[0], $pages[1]);

		$p = $_GET['p'];
			if(in_array($p.'.php', $pages)){
				include($pages_dir.'/'.$p.'.php');
			} else {
				include "404.php";
			}
		} else {
			include($pages_dir.'/home.php');
		}
	?>
		
   <?php 
   if(isset($_SESSION['usname'])) {
		require_once "pages/footer.php"; 
	}else{
		require_once "pages/footer_login.php"; 
	}
	?>
</body>
</html>